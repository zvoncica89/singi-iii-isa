package Banka;


@javax.persistence.Entity

@Table(name = "transakcija_racun")
public class TransakcijaRacun {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "transakcija_racun_id")
    private Long id;
        
    private Double iznos;
        
    private LocalDateTime datumValute;
        
    private int pozivNaBroj;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "racun_id")
    private Racun racun;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Double getIznos () {
        return this.iznos;
    }

    public void setIznos (Double iznos) {
        this.iznos = iznos;
    }
    
    public LocalDateTime getDatumValute () {
        return this.datumValute;
    }

    public void setDatumValute (LocalDateTime datumValute) {
        this.datumValute = datumValute;
    }
    
    public int getPozivNaBroj () {
        return this.pozivNaBroj;
    }

    public void setPozivNaBroj (int pozivNaBroj) {
        this.pozivNaBroj = pozivNaBroj;
    }
    
    public Racun getRacun () {
        return this.racun;
    }

    public void setRacun (Racun racun) {
        this.racun = racun;
    }
    


}