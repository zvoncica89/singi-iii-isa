package StudentskaSluzba;


@javax.persistence.Entity

@Table(name = "student")
public class Student {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;
        
    private String indeks;
        
    private String ime;
        
    private String prezime;
        
    private String email;
        
    private String lozinka;
        
    @javax.persistence.OneToOne(mappedBy="student")
    private FinansijskaKartica finansijskaKartica;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getIndeks () {
        return this.indeks;
    }

    public void setIndeks (String indeks) {
        this.indeks = indeks;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public String getEmail () {
        return this.email;
    }

    public void setEmail (String email) {
        this.email = email;
    }
    
    public String getLozinka () {
        return this.lozinka;
    }

    public void setLozinka (String lozinka) {
        this.lozinka = lozinka;
    }
    
    public FinansijskaKartica getFinansijskaKartica () {
        return this.finansijskaKartica;
    }

    public void setFinansijskaKartica (FinansijskaKartica finansijskaKartica) {
        this.finansijskaKartica = finansijskaKartica;
    }
    


}