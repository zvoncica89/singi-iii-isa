package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Racun;

@Controller
@RequestMapping(path = "/api/racun" )
public class RacunController extends HostCrudController<Racun, Long> {

	public RacunController() {
		this.category = "racun";
	}
}
