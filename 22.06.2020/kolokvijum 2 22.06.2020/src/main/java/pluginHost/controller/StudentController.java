package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Student;

@Controller
@RequestMapping(path = "/api/student" )
public class StudentController extends HostCrudController<Student, Long> {

	public StudentController() {
		this.category = "student";
	}
}
