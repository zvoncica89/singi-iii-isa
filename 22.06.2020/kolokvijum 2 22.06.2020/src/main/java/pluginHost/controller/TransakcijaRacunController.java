package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.TransakcijaRacun;

@Controller
@RequestMapping(path = "/api/transakcijaRacun" )
public class TransakcijaRacunController extends HostCrudController<TransakcijaRacun, Long> {
	public TransakcijaRacunController() {
		this.category = "transakcijaRacun";
	}
}
