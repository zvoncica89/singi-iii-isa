package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Transakcija;

@Controller
@RequestMapping(path = "/api/transakcija" )
public class TransakcijaController extends HostCrudController<Transakcija, Long> {
	public TransakcijaController() {
		this.category = "transakcija";
	}
}
