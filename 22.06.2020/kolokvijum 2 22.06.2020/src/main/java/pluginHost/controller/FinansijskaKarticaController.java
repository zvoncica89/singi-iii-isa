package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.FinansijskaKartica;

@Controller
@RequestMapping(path = "/api/finansijskaKartica" )
public class FinansijskaKarticaController extends HostCrudController<FinansijskaKartica, Long> {

	public FinansijskaKarticaController() {
		this.category = "finansijskaKartica";
	}
}
