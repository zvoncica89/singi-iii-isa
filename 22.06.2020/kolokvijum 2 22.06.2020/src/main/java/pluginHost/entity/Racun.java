package pluginHost.entity;

import javax.persistence.Column;
import javax.persistence.Table;



public class Racun implements Identitet{

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "racun_id")
    private Long id;
        
    private String brojRacuna;
        
    private Double stanje;
        
    @javax.persistence.OneToMany(mappedBy="racun")
    private java.util.List<TransakcijaRacun> transakcijaRacun;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getBrojRacuna () {
        return this.brojRacuna;
    }

    public void setBrojRacuna (String brojRacuna) {
        this.brojRacuna = brojRacuna;
    }
    
    public Double getStanje () {
        return this.stanje;
    }

    public void setStanje (Double stanje) {
        this.stanje = stanje;
    }
    
    public java.util.List<TransakcijaRacun> getTransakcijaRacun () {
        return this.transakcijaRacun;
    }

    public void setTransakcijaRacun (java.util.List<TransakcijaRacun> transakcijaRacun) {
        this.transakcijaRacun = transakcijaRacun;
    }

//	@Override
//	public RacunDTO getDTO(boolean isInsideDTO) {
//		RacunDTO racunDTO = new RacunDTO(this, isInsideDTO);
//		return racunDTO;
//	}
    


}