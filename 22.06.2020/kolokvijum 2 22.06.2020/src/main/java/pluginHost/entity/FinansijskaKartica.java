package pluginHost.entity;

import javax.persistence.Column;
import javax.persistence.Table;


@javax.persistence.Entity

@Table(name = "finansijska_kartica")
public class FinansijskaKartica implements Identitet{

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "finansijska_kartica_id")
    private Long id;
        
    private String pozivNaBroj;
        
    private Double stanje;
        
    @javax.persistence.OneToOne(mappedBy="finansijskaKartica")
    private Student student;
        
    @javax.persistence.OneToMany(mappedBy="finansijskaKartica")
    private java.util.List<Transakcija> transakcija;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getPozivNaBroj () {
        return this.pozivNaBroj;
    }

    public void setPozivNaBroj (String pozivNaBroj) {
        this.pozivNaBroj = pozivNaBroj;
    }
    
    public Double getStanje () {
        return this.stanje;
    }

    public void setStanje (Double stanje) {
        this.stanje = stanje;
    }
    
    public Student getStudent () {
        return this.student;
    }

    public void setStudent (Student student) {
        this.student = student;
    }
    
    public java.util.List<Transakcija> getTransakcija () {
        return this.transakcija;
    }

    public void setTransakcija (java.util.List<Transakcija> transakcija) {
        this.transakcija = transakcija;
    }

//	@Override
//	public FinansijskaKarticaDTO getDTO(boolean isInsideDTO) {
//		FinansijskaKarticaDTO finansijskaKarticaDTO = new FinansijskaKarticaDTO(this, isInsideDTO);
//		return finansijskaKarticaDTO;
//	}
//    


}