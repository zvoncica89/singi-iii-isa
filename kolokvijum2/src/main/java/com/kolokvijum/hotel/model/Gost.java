package com.kolokvijum.hotel.model;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity

@Table(name = "gost")
public class Gost {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "gost_id")
    private Long id;
        
    private String ime;
        
    private String prezime;
        
    private String email;
        
    private String lozinka;
        
    @javax.persistence.OneToMany(mappedBy="gost")
    private java.util.List<Rezervacija> rezervacija;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public String getEmail () {
        return this.email;
    }

    public void setEmail (String email) {
        this.email = email;
    }
    
    public String getLozinka () {
        return this.lozinka;
    }

    public void setLozinka (String lozinka) {
        this.lozinka = lozinka;
    }
    
    public java.util.List<Rezervacija> getRezervacija () {
        return this.rezervacija;
    }

    public void setRezervacija (java.util.List<Rezervacija> rezervacija) {
        this.rezervacija = rezervacija;
    }
    


    public Gost () {
        super();
        this.rezervacija = new java.util.ArrayList<>();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((ime == null) ? 0 : ime.hashCode());
		result = prime * result + ((lozinka == null) ? 0 : lozinka.hashCode());
		result = prime * result + ((prezime == null) ? 0 : prezime.hashCode());
		result = prime * result + ((rezervacija == null) ? 0 : rezervacija.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gost other = (Gost) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (ime == null) {
			if (other.ime != null)
				return false;
		} else if (!ime.equals(other.ime))
			return false;
		if (lozinka == null) {
			if (other.lozinka != null)
				return false;
		} else if (!lozinka.equals(other.lozinka))
			return false;
		if (prezime == null) {
			if (other.prezime != null)
				return false;
		} else if (!prezime.equals(other.prezime))
			return false;
		if (rezervacija == null) {
			if (other.rezervacija != null)
				return false;
		} else if (!rezervacija.equals(other.rezervacija))
			return false;
		return true;
	}
    
    

}