package com.kolokvijum.hotel.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;


@javax.persistence.Entity

@Table(name = "rezervacija")
public class Rezervacija {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "rezervacija_id")
    private Long id;
        
    private LocalDateTime odKad;
        
    private LocalDateTime doKad;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "gost_id")
    private Gost gost;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "soba_id")
    private Soba soba;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public LocalDateTime getOdKad () {
        return this.odKad;
    }

    public void setOdKad (LocalDateTime odKad) {
        this.odKad = odKad;
    }
    
    public LocalDateTime getDoKad () {
        return this.doKad;
    }

    public void setDoKad (LocalDateTime doKad) {
        this.doKad = doKad;
    }
    
    public Gost getGost () {
        return this.gost;
    }

    public void setGost (Gost gost) {
        this.gost = gost;
    }
    
    public Soba getSoba () {
        return this.soba;
    }

    public void setSoba (Soba soba) {
        this.soba = soba;
    }
    


    public Rezervacija () {
        super();
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doKad == null) ? 0 : doKad.hashCode());
		result = prime * result + ((gost == null) ? 0 : gost.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((odKad == null) ? 0 : odKad.hashCode());
		result = prime * result + ((soba == null) ? 0 : soba.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (doKad == null) {
			if (other.doKad != null)
				return false;
		} else if (!doKad.equals(other.doKad))
			return false;
		if (gost == null) {
			if (other.gost != null)
				return false;
		} else if (!gost.equals(other.gost))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (odKad == null) {
			if (other.odKad != null)
				return false;
		} else if (!odKad.equals(other.odKad))
			return false;
		if (soba == null) {
			if (other.soba != null)
				return false;
		} else if (!soba.equals(other.soba))
			return false;
		return true;
	}

    
}