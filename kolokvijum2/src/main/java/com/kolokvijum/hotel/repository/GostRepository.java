package com.kolokvijum.hotel.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kolokvijum.hotel.model.Gost;

@Repository
public interface GostRepository extends CrudRepository<Gost, Long> {
	Optional<Gost> getByIme(String ime);
	Optional<Gost> getByImeAndLozinka(String ime, String lozinka);

}
