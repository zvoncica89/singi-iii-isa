package com.kolokvijum.hotel.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.model.Rezervacija;
import com.kolokvijum.hotel.model.Soba;


public class RezervacijaDTO {
	private Long id;
	private LocalDateTime odKad;
	private LocalDateTime doKad;
	
	private Gost gost;
	private Soba soba;
	
	public RezervacijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RezervacijaDTO(Long id, LocalDateTime odKad, LocalDateTime doKad, Gost gost, Soba soba) {
		super();
		this.id = id;
		this.odKad = odKad;
		this.doKad = doKad;
		this.gost = gost;
		this.soba = soba;
	}

	public RezervacijaDTO(Rezervacija rezervacija) {
		this.id = rezervacija.getId();
		this.odKad = rezervacija.getOdKad();
		this.doKad = rezervacija.getDoKad();
		this.gost = rezervacija.getGost();
		this.soba = rezervacija.getSoba();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getOdKad() {
		return odKad;
	}

	public void setOdKad(LocalDateTime odKad) {
		this.odKad = odKad;
	}

	public LocalDateTime getDoKad() {
		return doKad;
	}

	public void setDoKad(LocalDateTime doKad) {
		this.doKad = doKad;
	}

	public Gost getGost() {
		return gost;
	}

	public void setGost(Gost gost) {
		this.gost = gost;
	}

	public Soba getSoba() {
		return soba;
	}

	public void setSoba(Soba soba) {
		this.soba = soba;
	}
	
	
	
	

}
