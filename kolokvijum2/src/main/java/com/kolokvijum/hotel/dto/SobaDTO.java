package com.kolokvijum.hotel.dto;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.kolokvijum.hotel.model.Rezervacija;
import com.kolokvijum.hotel.model.Soba;

public class SobaDTO {
	private Long id;
	private String naziv;
	private String opis;
	
	private List<RezervacijaDTO> rezervacije = new ArrayList<RezervacijaDTO>();

	public SobaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SobaDTO(Long id, String naziv, String opis, List<RezervacijaDTO> rezervacije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.rezervacije = rezervacije;
	}
	
	public SobaDTO(Soba soba) {
		super();
		this.id = soba.getId();
		this.naziv = soba.getNaziv();
		this.opis = soba.getOpis();
		for (Rezervacija r : soba.getRezervacija()) {
			rezervacije.add(new RezervacijaDTO(r.getId(), r.getOdKad(), r.getDoKad(), r.getGost(), r.getSoba()));
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public List<RezervacijaDTO> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(List<RezervacijaDTO> rezervacije) {
		this.rezervacije = rezervacije;
	}
	

}
