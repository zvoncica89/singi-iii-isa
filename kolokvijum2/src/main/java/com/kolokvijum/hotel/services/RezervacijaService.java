package com.kolokvijum.hotel.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.model.Rezervacija;
import com.kolokvijum.hotel.repository.RezervacijaRepository;

@Service
public class RezervacijaService {
	@Autowired
	RezervacijaRepository rezervacijaRepository;
	
	public RezervacijaService() {
		super();
	}
	
	public List<Rezervacija> findAll(){
		return Lists.newArrayList(rezervacijaRepository.findAll());
	}
	
	public Optional<Rezervacija> findById(Long id) {
		return rezervacijaRepository.findById(id);
	}
	
	public void delete(Rezervacija rezervacija) {
		rezervacijaRepository.delete(rezervacija);
	}
	
	public void deleteById(Rezervacija rezervacija) {
		rezervacijaRepository.deleteById(rezervacija.getId());
	}
	
	public Rezervacija save(Rezervacija rezervacija) {
		return rezervacijaRepository.save(rezervacija);
	}
	
	
	public List<Rezervacija> findMyReservations(Gost gost){
		List<Rezervacija> potrebneRezervacije = new ArrayList<Rezervacija>();
		List<Rezervacija> rezervacije = Lists.newArrayList(rezervacijaRepository.findAll());
		for (Rezervacija r : rezervacije) {
			if (r.getGost().equals(gost)) {
				potrebneRezervacije.add(r);
			}
		}
		return potrebneRezervacije;
		
	}
}
