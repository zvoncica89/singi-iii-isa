package com.kolokvijum.hotel.services;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.repository.GostRepository;

@Service
public class GostService {
	@Autowired
	GostRepository gostRepository;
	
	public GostService() {
		super();
	}
	
	public List<Gost> findAll(){
		return Lists.newArrayList(gostRepository.findAll());
	}
	
	public Optional<Gost> findById(Long id) {
		return gostRepository.findById(id);
	}
	
	public void delete(Gost gost) {
		gostRepository.delete(gost);
	}
	
	public void deleteById(Gost gost) {
		gostRepository.deleteById(gost.getId());
	}
	
	public Gost save(Gost gost) {
		return gostRepository.save(gost);
	}
	
	public Gost findByName(String ime) {
		List<Gost> gosti = findAll();
		for (Gost g : gosti) {
			if (g.getIme().equals(ime)) {
				return g;
			}
		}
		return null;
		
	}
}
