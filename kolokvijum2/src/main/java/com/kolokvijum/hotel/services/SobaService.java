package com.kolokvijum.hotel.services;

import java.util.List;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.springframework.beans.factory.annotation.Autowired;

import com.kolokvijum.hotel.model.Rezervacija;
import com.kolokvijum.hotel.model.Soba;
import com.kolokvijum.hotel.repository.RezervacijaRepository;
import com.kolokvijum.hotel.repository.SobaRepository;

public class SobaService {
	@Autowired
	SobaRepository sobaRepository;
	
	public SobaService() {
		super();
	}
	
	public List<Soba> findAll(){
		return Lists.newArrayList(sobaRepository.findAll());
	}
	
	public Optional<Soba> findById(Long id) {
		return sobaRepository.findById(id);
	}
	
	public void delete(Soba soba) {
		sobaRepository.delete(soba);
	}
	
	public void deleteById(Soba soba) {
		sobaRepository.deleteById(soba.getId());
	}
	
	public Soba save(Soba soba) {
		return sobaRepository.save(soba);
	}

}
