package com.kolokvijum.hotel.services;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.kolokvijum.hotel.model.Gost;


@Service
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	UserService userService;
	
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String ime) throws UsernameNotFoundException {
		Optional<Gost> gost = userService.getUser(ime);
		if(gost.isPresent()) {
			ArrayList<GrantedAuthority> grantedAuthorities = new ArrayList<GrantedAuthority>();
			grantedAuthorities.add(new SimpleGrantedAuthority("gost"));
			return new org.springframework.security.core.userdetails.User(gost.get().getIme(), gost.get().getLozinka(), grantedAuthorities);
		}
		
		return null;
	}
	

}
