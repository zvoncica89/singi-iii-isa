package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Vlasnik;

@Controller
@RequestMapping(path = "api/vlasnik")
@CrossOrigin(origins = "*")
public class VlasnikController extends HostCrudController<Vlasnik, Long>{
	public VlasnikController() {
		this.category = "vlasnik";
	}

}
