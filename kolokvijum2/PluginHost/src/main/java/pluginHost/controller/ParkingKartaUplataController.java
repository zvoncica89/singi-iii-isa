package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.ParkingKartaUplata;

@Controller
@RequestMapping(path = "/api/parkingKartaUplata")
@CrossOrigin(origins = "*")
public class ParkingKartaUplataController extends HostCrudController<ParkingKartaUplata, Long> {
	public ParkingKartaUplataController() {
		this.category = "parkingKartaUplata";
	}
}
