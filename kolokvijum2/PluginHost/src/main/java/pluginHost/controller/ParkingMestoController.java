package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.ParkingMesto;

@Controller
@RequestMapping(path = "/api/parkingMesto")
@CrossOrigin(origins = "*")
public class ParkingMestoController extends HostCrudController<ParkingMesto, Long> {
	public ParkingMestoController() {
		this.category = "parkingMesto";
	}

}
