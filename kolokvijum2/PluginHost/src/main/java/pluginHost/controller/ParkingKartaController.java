package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.ParkingKarta;

@Controller
@RequestMapping(path = "api/parkingKarta")
@CrossOrigin(origins = "*")
public class ParkingKartaController extends HostCrudController<ParkingKarta, Long> {
	public ParkingKartaController() {
		this.category = "parkingKarta";
	}

}
