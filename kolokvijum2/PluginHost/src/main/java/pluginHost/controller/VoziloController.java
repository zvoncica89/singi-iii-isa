package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Vozilo;

@Controller
@RequestMapping(path = "api/vozilo")
@CrossOrigin(origins = "*")
public class VoziloController extends HostCrudController<Vozilo, Long>{
	
	public VoziloController() {
		this.category = "vozilo";
	} 
}
