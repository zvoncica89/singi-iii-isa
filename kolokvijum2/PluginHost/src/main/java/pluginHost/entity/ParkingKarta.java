package pluginHost.entity;

import java.time.LocalDateTime;




public class ParkingKarta implements Identitet {

    private Long id;
        
    private Long parkingMesto;
        
    private LocalDateTime pocetakVazenja;
        
    private LocalDateTime krajVazenja;

    private Vozilo vozilo;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Long getParkingMesto () {
        return this.parkingMesto;
    }

    public void setParkingMesto (Long parkingMesto) {
        this.parkingMesto = parkingMesto;
    }
    
    public LocalDateTime getPocetakVazenja () {
        return this.pocetakVazenja;
    }

    public void setPocetakVazenja (LocalDateTime pocetakVazenja) {
        this.pocetakVazenja = pocetakVazenja;
    }
    
    public LocalDateTime getKrajVazenja () {
        return this.krajVazenja;
    }

    public void setKrajVazenja (LocalDateTime krajVazenja) {
        this.krajVazenja = krajVazenja;
    }
    
    public Vozilo getVozilo () {
        return this.vozilo;
    }

    public void setVozilo (Vozilo vozilo) {
        this.vozilo = vozilo;
    }
    
    

	public ParkingKarta() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

	public ParkingKarta(Long id, Long parkingMesto, LocalDateTime pocetakVazenja, LocalDateTime krajVazenja,
			Vozilo vozilo) {
		super();
		this.id = id;
		this.parkingMesto = parkingMesto;
		this.pocetakVazenja = pocetakVazenja;
		this.krajVazenja = krajVazenja;
		this.vozilo = vozilo;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((krajVazenja == null) ? 0 : krajVazenja.hashCode());
		result = prime * result + ((parkingMesto == null) ? 0 : parkingMesto.hashCode());
		result = prime * result + ((pocetakVazenja == null) ? 0 : pocetakVazenja.hashCode());
		result = prime * result + ((vozilo == null) ? 0 : vozilo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParkingKarta other = (ParkingKarta) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (krajVazenja == null) {
			if (other.krajVazenja != null)
				return false;
		} else if (!krajVazenja.equals(other.krajVazenja))
			return false;
		if (parkingMesto == null) {
			if (other.parkingMesto != null)
				return false;
		} else if (!parkingMesto.equals(other.parkingMesto))
			return false;
		if (pocetakVazenja == null) {
			if (other.pocetakVazenja != null)
				return false;
		} else if (!pocetakVazenja.equals(other.pocetakVazenja))
			return false;
		if (vozilo == null) {
			if (other.vozilo != null)
				return false;
		} else if (!vozilo.equals(other.vozilo))
			return false;
		return true;
	}

    


}