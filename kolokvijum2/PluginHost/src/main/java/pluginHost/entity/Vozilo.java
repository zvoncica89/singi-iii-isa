package pluginHost.entity;



@javax.persistence.Entity

public class Vozilo implements Identitet {

	private Long id;

	private String registracioniBroj;

	private Vlasnik vlasnik;
	private java.util.List<ParkingKarta> parkignKarta;



	public Long getId () {
		return this.id;
	}

	public void setId (Long id) {
		this.id = id;
	}

	public String getRegistracioniBroj () {
		return this.registracioniBroj;
	}

	public void setRegistracioniBroj (String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}

	public Vlasnik getVlasnik () {
		return this.vlasnik;
	}

	public void setVlasnik (Vlasnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public java.util.List<ParkingKarta> getParkignKarta () {
		return this.parkignKarta;
	}

	public void setParkignKarta (java.util.List<ParkingKarta> parkignKarta) {
		this.parkignKarta = parkignKarta;
	}






}