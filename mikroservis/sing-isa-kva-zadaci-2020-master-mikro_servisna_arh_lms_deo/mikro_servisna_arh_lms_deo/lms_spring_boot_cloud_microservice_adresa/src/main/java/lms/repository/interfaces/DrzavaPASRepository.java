package lms.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lms.model.Drzava;

@Repository
@Scope("singleton")
public interface DrzavaPASRepository extends PagingAndSortingRepository<Drzava, Long> {
    
    @Query("select d from Drzava d where d.naziv LIKE :naziv%")
    public Page<Drzava> findByNaziv (@Param("naziv") String naziv, Pageable pageable);

}