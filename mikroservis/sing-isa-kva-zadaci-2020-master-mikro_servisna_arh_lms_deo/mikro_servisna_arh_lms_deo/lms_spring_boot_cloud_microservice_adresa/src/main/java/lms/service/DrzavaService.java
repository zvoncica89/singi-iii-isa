package lms.service;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lms.model.Drzava;
import lms.repository.interfaces.DrzavaPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class DrzavaService extends CrudService<Drzava, Long> {
    
    public Page<Drzava> findByNaziv (String naziv, Pageable pageable) {
	return ((DrzavaPASRepository)this.repository.getRepository()).findByNaziv(naziv, pageable);
    }

}