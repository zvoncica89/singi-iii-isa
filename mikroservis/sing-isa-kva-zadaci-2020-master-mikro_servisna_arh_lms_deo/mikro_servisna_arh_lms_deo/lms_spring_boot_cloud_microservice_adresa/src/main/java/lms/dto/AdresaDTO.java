package lms.dto;

import lms.dto.interfaces.DTO;
import lms.enums.StanjeModela;
import lms.enums.TipAdrese;

public class AdresaDTO implements DTO<Long> {

    /**
     * 
     */
    private static final long serialVersionUID = 6239772868351411140L;

    private Long id;

    private String ulica;

    private String broj;

    private StanjeModela stanje;

    private Long version;

    private TipAdrese tip;

    private GradDTO gradDTO;

    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getUlica() {
	return this.ulica;
    }

    public void setUlica(String ulica) {
	this.ulica = ulica;
    }

    public String getBroj() {
	return this.broj;
    }

    public void setBroj(String broj) {
	this.broj = broj;
    }

    public StanjeModela getStanje() {
	return this.stanje;
    }

    public void setStanje(StanjeModela stanje) {
	this.stanje = stanje;
    }

    public Long getVersion() {
	return this.version;
    }

    public void setVersion(Long version) {
	this.version = version;
    }

    public TipAdrese getTip() {
	return this.tip;
    }

    public void setTip(TipAdrese tip) {
	this.tip = tip;
    }

    public GradDTO getGradDTO() {
	return this.gradDTO;
    }

    public void setGradDTO(GradDTO gradDTO) {
	this.gradDTO = gradDTO;
    }

    public AdresaDTO() {
	super();
    }

}