package lms.repository.interfaces;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import lms.model.Grad;

@Repository
@Scope("singleton")
public interface GradPASRepository extends PagingAndSortingRepository<Grad, Long> {
    
    @Query("select g from Grad g where g.drzava.id = :id")
    public Page<Grad> findByDrzavaId(@Param("id") Long id, Pageable pageable);

}