package lms.interfaces;

import java.io.Serializable;

public interface Identifikacija<ID> extends Serializable {

    public ID getId();

    public void setId(ID id);

}