package lms.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lms.controller.CrudController;
import lms.dto.GradDTO;
import lms.model.Grad;
import lms.service.GradService;

@Controller
@Scope("singleton")
@RequestMapping(path = "/grad")
@CrossOrigin(origins = "*")
public class GradController extends CrudController<Grad, Long> {

    @RequestMapping(path = "/drzava/{id}", method = RequestMethod.GET)
    public ResponseEntity<Page<GradDTO>> findByCountryId (@PathVariable("id") Long id, Pageable pageable) {
	
//	get Page<T>
	Page<Grad> page = ((GradService)this.service).findByDrzavaId(id, pageable);

//	map T to DTO and get Page<DTO>
	Page<GradDTO> pageDTO = page.map(t -> t.getDTO());
	
	return new ResponseEntity<Page<GradDTO>>(pageDTO, HttpStatus.OK);
    }
    
}