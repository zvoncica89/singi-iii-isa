package lms.service;

import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import lms.model.Grad;
import lms.repository.interfaces.GradPASRepository;
import lms.service.CrudService;

@Service
@Scope("singleton")
public class GradService extends CrudService<Grad, Long> {
    
    public Page<Grad> findByDrzavaId(Long id, Pageable pageable) {
	return ((GradPASRepository)this.repository.getRepository()).findByDrzavaId(id, pageable);
    }

}