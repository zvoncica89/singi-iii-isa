package banka.model;

import banka.DTO.DTO;

public interface CreateDTO<ID> {
	public DTO getDTO(boolean isInsideDTO);
	public ID getId();

}
