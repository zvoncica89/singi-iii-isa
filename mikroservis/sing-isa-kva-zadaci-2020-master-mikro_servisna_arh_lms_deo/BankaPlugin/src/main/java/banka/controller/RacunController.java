package banka.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import banka.model.Racun;

@Controller
@RequestMapping(path = "/api/racun")
@CrossOrigin(origins = "*")
public class RacunController extends CRUDController<Racun, Long> {

}
