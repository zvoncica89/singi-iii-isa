package banka.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import banka.model.TransakcijaRacun;

@Controller
@RequestMapping(path = "/api/transakcijaRacun")
@CrossOrigin(origins = "*")
public class TransakcijaRacunController extends CRUDController<TransakcijaRacun, Long> {

}
