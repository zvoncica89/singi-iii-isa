package banka.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import banka.model.Racun;

@Repository
public interface RacunRepository extends PagingAndSortingRepository<Racun, Long> {

}
