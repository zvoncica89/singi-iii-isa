package banka.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import banka.model.TransakcijaRacun;

@Repository
public interface TransakcijaRacunRepository extends PagingAndSortingRepository<TransakcijaRacun, Long> {

}
