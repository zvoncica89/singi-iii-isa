package banka.DTO;

import java.util.List;
import java.util.stream.Collectors;

import banka.model.Racun;


public class RacunDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;

	private String brojRacuna;

	private Double stanje;

	private List<TransakcijaRacunDTO> transakcijaRacun;

	public RacunDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RacunDTO(Long id, String brojRacuna, Double stanje, List<TransakcijaRacunDTO> transakcijaRacun) {
		super();
		this.id = id;
		this.brojRacuna = brojRacuna;
		this.stanje = stanje;
		this.transakcijaRacun = transakcijaRacun;
	}

	public RacunDTO(Racun racun, boolean isInsideDTO) {
		this.id = racun.getId();
		this.brojRacuna = racun.getBrojRacuna();
		this.stanje = racun.getStanje();
		if (!isInsideDTO) {
			this.transakcijaRacun = racun.getTransakcijaRacun().stream().map(t -> t.getDTO(!isInsideDTO)).collect(Collectors.toList());
		}
	}

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getBrojRacuna() {
		return this.brojRacuna;
	}

	public void setBrojRacuna(String brojRacuna) {
		this.brojRacuna = brojRacuna;
	}

	public Double getStanje() {
		return this.stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public java.util.List<TransakcijaRacunDTO> getTransakcijaRacun() {
		return this.transakcijaRacun;
	}

	public void setTransakcijaRacun(java.util.List<TransakcijaRacunDTO> transakcijaRacun) {
		this.transakcijaRacun = transakcijaRacun;
	}

}