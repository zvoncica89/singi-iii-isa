package banka.DTO;

import java.time.LocalDateTime;

import banka.model.TransakcijaRacun;


public class TransakcijaRacunDTO implements DTO {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
        
    private Double iznos;
        
    private LocalDateTime datumValute;
        
    private int pozivNaBroj;
        
    private RacunDTO racun;
    public TransakcijaRacunDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
    

	public TransakcijaRacunDTO(Long id, Double iznos, LocalDateTime datumValute, int pozivNaBroj, RacunDTO racun) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
		this.pozivNaBroj = pozivNaBroj;
		this.racun = racun;
	}
	
	public TransakcijaRacunDTO(TransakcijaRacun transakcijaRacun, boolean isInsideDTO) {
		this.id = transakcijaRacun.getId();
		this.iznos = transakcijaRacun.getIznos();
		this.datumValute = transakcijaRacun.getDatumValute();
		this.pozivNaBroj = transakcijaRacun.getPozivNaBroj();
		if (!isInsideDTO) {
			this.racun = transakcijaRacun.getRacun().getDTO(!isInsideDTO);
		}
	}


	public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Double getIznos () {
        return this.iznos;
    }

    public void setIznos (Double iznos) {
        this.iznos = iznos;
    }
    
    public LocalDateTime getDatumValute () {
        return this.datumValute;
    }

    public void setDatumValute (LocalDateTime datumValute) {
        this.datumValute = datumValute;
    }
    
    public int getPozivNaBroj () {
        return this.pozivNaBroj;
    }

    public void setPozivNaBroj (int pozivNaBroj) {
        this.pozivNaBroj = pozivNaBroj;
    }
    
    public RacunDTO getRacun () {
        return this.racun;
    }

    public void setRacun (RacunDTO racun) {
        this.racun = racun;
    }
    


}