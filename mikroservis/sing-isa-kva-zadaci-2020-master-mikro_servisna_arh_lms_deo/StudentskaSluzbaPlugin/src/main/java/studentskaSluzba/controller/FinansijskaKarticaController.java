package studentskaSluzba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import studentskaSluzba.model.FinansijskaKartica;
import studentskaSluzba.service.CRUDService;

@Controller
@RequestMapping(path = "/api/finansijskaKartica")
@CrossOrigin(origins = "*")
public class FinansijskaKarticaController extends CRUDService<FinansijskaKartica, Long> {

}
