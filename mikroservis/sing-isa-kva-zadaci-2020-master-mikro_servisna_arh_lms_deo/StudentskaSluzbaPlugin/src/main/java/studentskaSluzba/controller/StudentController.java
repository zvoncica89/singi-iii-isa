package studentskaSluzba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import studentskaSluzba.model.Student;

@Controller
@RequestMapping(path = "/api/student")
@CrossOrigin(origins = "*")
public class StudentController extends CRUDController<Student, Long> {

}
