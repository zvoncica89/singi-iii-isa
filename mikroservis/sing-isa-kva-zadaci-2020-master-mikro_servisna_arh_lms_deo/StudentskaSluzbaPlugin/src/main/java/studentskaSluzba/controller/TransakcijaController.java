package studentskaSluzba.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import studentskaSluzba.model.Transakcija;

@Controller
@RequestMapping(path = "/api/transakcija")
@CrossOrigin(origins = "*")
public class TransakcijaController extends CRUDController<Transakcija, Long> {

}
