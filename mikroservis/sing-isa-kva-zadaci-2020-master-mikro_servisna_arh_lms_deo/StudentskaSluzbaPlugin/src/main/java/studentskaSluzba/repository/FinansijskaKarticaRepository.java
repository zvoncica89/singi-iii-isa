package studentskaSluzba.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import studentskaSluzba.model.FinansijskaKartica;
@Repository
public interface FinansijskaKarticaRepository extends PagingAndSortingRepository<FinansijskaKartica, Long> {

}
