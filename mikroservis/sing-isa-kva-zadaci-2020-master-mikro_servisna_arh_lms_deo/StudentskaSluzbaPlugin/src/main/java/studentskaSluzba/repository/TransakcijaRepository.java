package studentskaSluzba.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import studentskaSluzba.model.Transakcija;

@Repository
public interface TransakcijaRepository extends PagingAndSortingRepository<Transakcija, Long> {

}
