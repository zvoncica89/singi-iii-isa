package studentskaSluzba.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import studentskaSluzba.model.Student;

@Repository
public interface StudentRepository extends PagingAndSortingRepository<Student, Long>{

}
