package studentskaSluzba.DTO;

import java.time.LocalDateTime;

import studentskaSluzba.model.Transakcija;


public class TransakcijaDTO implements DTO {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
        
    private Double iznos;
        
    private LocalDateTime datumValute;
        
    private FinansijskaKarticaDTO finansijskaKartica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getIznos() {
		return iznos;
	}

	public void setIznos(Double iznos) {
		this.iznos = iznos;
	}

	public LocalDateTime getDatumValute() {
		return datumValute;
	}

	public void setDatumValute(LocalDateTime datumValute) {
		this.datumValute = datumValute;
	}

	public FinansijskaKarticaDTO getFinansijskaKartica() {
		return finansijskaKartica;
	}

	public void setFinansijskaKartica(FinansijskaKarticaDTO finansijskaKartica) {
		this.finansijskaKartica = finansijskaKartica;
	}

	public TransakcijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransakcijaDTO(Long id, Double iznos, LocalDateTime datumValute, FinansijskaKarticaDTO finansijskaKartica) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
		this.finansijskaKartica = finansijskaKartica;
	}
	
	public TransakcijaDTO(Transakcija transakcija, boolean isInsideDTO) {
		this.id = transakcija.getId();
		this.iznos = transakcija.getIznos();
		this.datumValute = transakcija.getDatumValute();
		if (!isInsideDTO) {
			this.finansijskaKartica = transakcija.getFinansijskaKartica().getDTO(!isInsideDTO);
		}
		
	}
    
    
}
