package studentskaSluzba.DTO;

import java.util.List;
import java.util.stream.Collectors;

import studentskaSluzba.model.FinansijskaKartica;

public class FinansijskaKarticaDTO implements DTO {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
        
    private String pozivNaBroj;
        
    private Double stanje;
        
    private StudentDTO student;
        
    private java.util.List<TransakcijaDTO> transakcija;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPozivNaBroj() {
		return pozivNaBroj;
	}

	public void setPozivNaBroj(String pozivNaBroj) {
		this.pozivNaBroj = pozivNaBroj;
	}

	public Double getStanje() {
		return stanje;
	}

	public void setStanje(Double stanje) {
		this.stanje = stanje;
	}

	public StudentDTO getStudent() {
		return student;
	}

	public void setStudent(StudentDTO student) {
		this.student = student;
	}

	public java.util.List<TransakcijaDTO> getTransakcija() {
		return transakcija;
	}

	public void setTransakcija(java.util.List<TransakcijaDTO> transakcija) {
		this.transakcija = transakcija;
	}

	public FinansijskaKarticaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinansijskaKarticaDTO(Long id, String pozivNaBroj, Double stanje, StudentDTO student,
			List<TransakcijaDTO> transakcija) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.student = student;
		this.transakcija = transakcija;
	}
	
	public FinansijskaKarticaDTO(FinansijskaKartica finansijskaKartica, boolean isInsideDTO) {
		super();
		this.id = finansijskaKartica.getId();
		this.pozivNaBroj = finansijskaKartica.getPozivNaBroj();
		this.stanje = finansijskaKartica.getStanje();
		if (!isInsideDTO) {
			this.student = finansijskaKartica.getStudent().getDTO(!isInsideDTO);
			this.transakcija = finansijskaKartica.getTransakcija().stream().map(f -> f.getDTO(!isInsideDTO)).collect(Collectors.toList());
		}
	}
    
    

}
