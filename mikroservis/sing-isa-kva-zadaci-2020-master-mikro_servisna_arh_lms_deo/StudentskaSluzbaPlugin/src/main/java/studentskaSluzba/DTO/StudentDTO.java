package studentskaSluzba.DTO;

import studentskaSluzba.model.Student;

public class StudentDTO implements DTO {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
        
    private String indeks;
        
    private String ime;
        
    private String prezime;
        
    private String email;
        
    private String lozinka;
        
    private FinansijskaKarticaDTO finansijskaKartica;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIndeks() {
		return indeks;
	}

	public void setIndeks(String indeks) {
		this.indeks = indeks;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getLozinka() {
		return lozinka;
	}

	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}

	public FinansijskaKarticaDTO getFinansijskaKartica() {
		return finansijskaKartica;
	}

	public void setFinansijskaKartica(FinansijskaKarticaDTO finansijskaKartica) {
		this.finansijskaKartica = finansijskaKartica;
	}

	public StudentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StudentDTO(Long id, String indeks, String ime, String prezime, String email, String lozinka,
			FinansijskaKarticaDTO finansijskaKartica) {
		super();
		this.id = id;
		this.indeks = indeks;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.lozinka = lozinka;
		this.finansijskaKartica = finansijskaKartica;
	}
	
	public StudentDTO(Student student, boolean isInsideDTO) {
		this.id = student.getId();
		this.indeks = student.getIndeks();
		this.ime = student.getIme();
		this.prezime = student.getPrezime();
		this.email = student.getPrezime();
		this.lozinka = student.getLozinka();
		if (!isInsideDTO) {
			this.finansijskaKartica = student.getFinansijskaKartica().getDTO(!isInsideDTO);
		}
	}
	
	
    
    

}
