package studentskaSluzba.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String title;

    @OneToMany(mappedBy = "role")
    private Set<StudentRole> studentRoles;

    public Role() {

    }

    public Role(Long id, String title, Set<StudentRole> studentiRola) {
	super();
	this.id = id;
	this.title = title;
	this.studentRoles = studentiRola;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getTitle() {
	return title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    public Set<StudentRole> getStudentiRola() {
	return studentRoles;
    }

    public void setStudentiRola(Set<StudentRole> studentiRola) {
	this.studentRoles = studentiRola;
    }

}
