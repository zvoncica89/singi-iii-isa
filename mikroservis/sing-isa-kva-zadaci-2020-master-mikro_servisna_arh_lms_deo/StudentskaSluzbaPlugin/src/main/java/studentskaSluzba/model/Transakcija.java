package studentskaSluzba.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import studentskaSluzba.DTO.TransakcijaDTO;

@javax.persistence.Entity

@Table(name = "transakcija")
public class Transakcija implements CreateDTO<Long>, Identitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "transakcija_id")
    private Long id;
        
    private Double iznos;
        
    private LocalDateTime datumValute;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "finansijska_kartica_id")
    private FinansijskaKartica finansijskaKartica;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Double getIznos () {
        return this.iznos;
    }

    public void setIznos (Double iznos) {
        this.iznos = iznos;
    }
    
    public LocalDateTime getDatumValute () {
        return this.datumValute;
    }

    public void setDatumValute (LocalDateTime datumValute) {
        this.datumValute = datumValute;
    }
    
    public FinansijskaKartica getFinansijskaKartica () {
        return this.finansijskaKartica;
    }

    public void setFinansijskaKartica (FinansijskaKartica finansijskaKartica) {
        this.finansijskaKartica = finansijskaKartica;
    }

	public Transakcija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Transakcija(Long id, Double iznos, LocalDateTime datumValute, FinansijskaKartica finansijskaKartica) {
		super();
		this.id = id;
		this.iznos = iznos;
		this.datumValute = datumValute;
		this.finansijskaKartica = finansijskaKartica;
	}
    
	@Override
	public TransakcijaDTO getDTO(boolean isInsideDTO) {
		TransakcijaDTO transakcijaDTO = new TransakcijaDTO(this, isInsideDTO);
		return transakcijaDTO;
	}
    
    


}