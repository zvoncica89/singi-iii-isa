package studentskaSluzba.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import studentskaSluzba.DTO.FinansijskaKarticaDTO;

@javax.persistence.Entity

@Table(name = "finansijska_kartica")
public class FinansijskaKartica implements CreateDTO<Long>, Identitet{

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "finansijska_kartica_id")
    private Long id;
        
    private String pozivNaBroj;
        
    private Double stanje;
        
//    @javax.persistence.OneToOne(mappedBy="finansijskaKartica")
    @OneToOne
    private Student student;
        
    @javax.persistence.OneToMany(mappedBy="finansijskaKartica")
    private java.util.List<Transakcija> transakcija;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getPozivNaBroj () {
        return this.pozivNaBroj;
    }

    public void setPozivNaBroj (String pozivNaBroj) {
        this.pozivNaBroj = pozivNaBroj;
    }
    
    public Double getStanje () {
        return this.stanje;
    }

    public void setStanje (Double stanje) {
        this.stanje = stanje;
    }
    
    public Student getStudent () {
        return this.student;
    }

    public void setStudent (Student student) {
        this.student = student;
    }
    
    public java.util.List<Transakcija> getTransakcija () {
        return this.transakcija;
    }

    public void setTransakcija (java.util.List<Transakcija> transakcija) {
        this.transakcija = transakcija;
    }

	public FinansijskaKartica() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FinansijskaKartica(Long id, String pozivNaBroj, Double stanje, Student student,
			List<Transakcija> transakcija) {
		super();
		this.id = id;
		this.pozivNaBroj = pozivNaBroj;
		this.stanje = stanje;
		this.student = student;
		this.transakcija = transakcija;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((pozivNaBroj == null) ? 0 : pozivNaBroj.hashCode());
		result = prime * result + ((stanje == null) ? 0 : stanje.hashCode());
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		result = prime * result + ((transakcija == null) ? 0 : transakcija.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinansijskaKartica other = (FinansijskaKartica) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (pozivNaBroj == null) {
			if (other.pozivNaBroj != null)
				return false;
		} else if (!pozivNaBroj.equals(other.pozivNaBroj))
			return false;
		if (stanje == null) {
			if (other.stanje != null)
				return false;
		} else if (!stanje.equals(other.stanje))
			return false;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		if (transakcija == null) {
			if (other.transakcija != null)
				return false;
		} else if (!transakcija.equals(other.transakcija))
			return false;
		return true;
	}

	@Override
	public FinansijskaKarticaDTO getDTO(boolean isInsideDTO) {
		FinansijskaKarticaDTO finansijskaKarticaDTO = new FinansijskaKarticaDTO(this, isInsideDTO);
		return finansijskaKarticaDTO;
	}
    
    


}