package studentskaSluzba.model;

import studentskaSluzba.DTO.DTO;

public interface CreateDTO<ID> {
	public DTO getDTO(boolean isInsideDTO);
	public ID getId();

}
