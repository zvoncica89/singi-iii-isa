package com.mina.hotel.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.mina.hotel.model.Gost;
import com.mina.hotel.model.Rezervacija;
import com.mina.hotel.repository.GostRepository;

@Service
public class GostService extends CRUDService<Gost, Long> {	
	public Page<Rezervacija> findRezervacijaByGostId(Long id, Pageable pageable){
		return ((GostRepository)repository).findRezervacijaByGostId(id, pageable);
	}
}
