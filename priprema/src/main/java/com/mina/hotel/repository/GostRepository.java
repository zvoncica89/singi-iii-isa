package com.mina.hotel.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mina.hotel.model.Gost;
import com.mina.hotel.model.Rezervacija;

@Repository
public interface GostRepository extends PagingAndSortingRepository<Gost, Long> {
	@Query("SELECT g from Gost g where g.ime = ime")
	public Page<Gost> findByIme(String ime, Pageable pageable);
	
	@Query("SELECT r from Rezervacija r where r.gost = ?1")
	public Page<Rezervacija> findRezervacijaByGostId(Long gostId, Pageable pageable);
}
