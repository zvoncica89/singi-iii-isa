package com.mina.hotel.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mina.hotel.model.Rezervacija;

@Repository
public interface RezervacijeRepository extends PagingAndSortingRepository<Rezervacija, Long> {

}
