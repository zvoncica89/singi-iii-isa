package com.mina.hotel.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.mina.hotel.model.Soba;
@Repository
public interface SobaRepository extends PagingAndSortingRepository<Soba, Long> {

}
