package com.mina.hotel.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.mina.hotel.model.Rezervacija;
import com.mina.hotel.model.Soba;

public class SobaDTO implements DTO {
	private Long id;
	private String naziv;
	private String opis;

	private List<RezervacijaDTO> rezervacije = new ArrayList<RezervacijaDTO>();

	public SobaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SobaDTO(Long id, String naziv, String opis, List<RezervacijaDTO> rezervacije) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.rezervacije = rezervacije;
	}

	public SobaDTO(Soba soba, boolean isDTOInsideDTO) {
		this.id = soba.getId();
		this.naziv = soba.getNaziv();
		this.opis = soba.getOpis();
		if (!isDTOInsideDTO) {
			this.rezervacije = soba.getRezervacija().stream().map(r -> r.getDTO(!isDTOInsideDTO))
					.collect(Collectors.toList());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public List<RezervacijaDTO> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(List<RezervacijaDTO> rezervacije) {
		this.rezervacije = rezervacije;
	}

}
