package com.mina.hotel.dto;

import java.util.List;
import java.util.stream.Collectors;

import com.mina.hotel.model.Gost;

public class GostDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String ime;
	private String prezime;
	private String email;
	private List<RezervacijaDTO> rezervacija;

	public GostDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GostDTO(Long id, String ime, String prezime, String email, List<RezervacijaDTO> rezervacija) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.rezervacija = rezervacija;
	}

	public GostDTO(Gost gost, boolean isDTOInsideDTO) {
		super();
		this.id = gost.getId();
		this.ime = gost.getIme();
		this.prezime = gost.getPrezime();
		this.email = gost.getEmail();
		if(!isDTOInsideDTO)
			this.rezervacija = gost.getRezervacija().stream().map(r -> r.getDTO(!isDTOInsideDTO)).collect(Collectors.toList());
		}
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<RezervacijaDTO> getRezervacija() {
		return rezervacija;
	}

	public void setRezervacija(List<RezervacijaDTO> rezervacija) {
		this.rezervacija = rezervacija;
	}

}
