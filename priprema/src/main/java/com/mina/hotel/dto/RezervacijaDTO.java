package com.mina.hotel.dto;

import java.time.LocalDateTime;

import com.mina.hotel.model.Gost;
import com.mina.hotel.model.Rezervacija;
import com.mina.hotel.model.Soba;

public class RezervacijaDTO implements DTO {
	private Long id;
	private LocalDateTime odKad;
	private LocalDateTime doKad;

	private GostDTO gost;
	private SobaDTO soba;

	public RezervacijaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RezervacijaDTO(Long id, LocalDateTime odKad, LocalDateTime doKad, GostDTO gost, SobaDTO soba) {
		super();
		this.id = id;
		this.odKad = odKad;
		this.doKad = doKad;
		this.gost = gost;
		this.soba = soba;
	}

	public RezervacijaDTO(Rezervacija rezervacija, boolean isInsideDto) {
		this.id = rezervacija.getId();
		this.odKad = rezervacija.getOdKad();
		this.doKad = rezervacija.getDoKad();
		if (!isInsideDto) {
			this.gost = rezervacija.getGost().getDTO(!isInsideDto);
			this.soba = rezervacija.getSoba().getDTO(!isInsideDto);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getOdKad() {
		return odKad;
	}

	public void setOdKad(LocalDateTime odKad) {
		this.odKad = odKad;
	}

	public LocalDateTime getDoKad() {
		return doKad;
	}

	public void setDoKad(LocalDateTime doKad) {
		this.doKad = doKad;
	}

	public GostDTO getGost() {
		return gost;
	}

	public void setGost(GostDTO gost) {
		this.gost = gost;
	}

	public SobaDTO getSoba() {
		return soba;
	}

	public void setSoba(SobaDTO soba) {
		this.soba = soba;
	}

}
