package com.mina.hotel.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mina.hotel.dto.DTO;
import com.mina.hotel.model.CreateDTO;
import com.mina.hotel.service.CRUDService;

public class CRUDController <T extends CreateDTO, ID> {
	@Autowired
	protected CRUDService<T, ID> crudService;
	
	public CRUDService<T, ID> getCrudService(){
		return this.crudService;
	}
	public void setCrudService(CRUDService<T, ID> crudService) {
		this.crudService = crudService;
	}
	
	@RequestMapping(path="", method = RequestMethod.GET)
	public ResponseEntity<Page<DTO>> findAll(Pageable pageable){
		Page<DTO> page = this.crudService.findAll(pageable).map(t -> t.getDTO(false));
		return new ResponseEntity<Page<DTO>> (page, HttpStatus.OK);
	}
	
	@RequestMapping(path="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(@PathVariable("id") ID id) {
		Optional<T> t = this.crudService.findOne(id);
		if(t.isPresent()) {
			return new ResponseEntity<DTO>(t.get().getDTO(false), HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
	}
	

}
