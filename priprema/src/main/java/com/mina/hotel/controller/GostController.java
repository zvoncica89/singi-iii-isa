package com.mina.hotel.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mina.hotel.dto.RezervacijaDTO;
import com.mina.hotel.model.Gost;
import com.mina.hotel.service.GostService;

@Controller
@RequestMapping(path = "/api/gost")
@CrossOrigin(origins = "*")
public class GostController extends CRUDController<Gost, Long> {
	
	@RequestMapping(path = "/mojeRezervacije/{id}", method = RequestMethod.GET)
	public ResponseEntity<Page<RezervacijaDTO>> findRezervacijaByGostId(@PathVariable("id") Long id, Pageable pageable){
		Page<RezervacijaDTO> mojeRezervacije = ((GostService)this.crudService).findRezervacijaByGostId(id, pageable).map(t -> t.getDTO(false));
		return new ResponseEntity<Page<RezervacijaDTO>> ( mojeRezervacije, HttpStatus.OK);
	}

}
