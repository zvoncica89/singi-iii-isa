package com.mina.hotel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mina.hotel.model.Soba;

@Controller
@RequestMapping(path = "/api/soba")
public class SobaController extends CRUDController<Soba, Long> {

}
