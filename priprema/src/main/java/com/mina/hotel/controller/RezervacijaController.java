package com.mina.hotel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mina.hotel.model.Rezervacija;

@Controller
@RequestMapping(path = "/api/rezervacija")
public class RezervacijaController extends CRUDController<Rezervacija, Long>{

}
