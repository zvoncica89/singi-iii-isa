package com.isa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isa.model.Korisnik;
import com.isa.service.KorisnikService;


@Controller
@RequestMapping(path = "/api/korisnici")
public class KorisnikController {
	@Autowired
	private KorisnikService korisnikService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<List<Korisnik>> getAllKorisnici(){
		return new ResponseEntity<List<Korisnik>>(korisnikService.findAll(), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{korisnickoIme}", method = RequestMethod.GET)
	public ResponseEntity<Korisnik> getOneKorisnik(@PathVariable("korisnickoIme") String korisnickoIme){
		return new ResponseEntity<Korisnik>(korisnikService.findOne(korisnickoIme), HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Korisnik> createKorisnik(@RequestBody Korisnik noviKorisnik){
		if (korisnikService.findOne(noviKorisnik.getKorisnickoIme()) != null) {
			return new ResponseEntity<Korisnik>(HttpStatus.CONFLICT);
		} else {
			korisnikService.save(noviKorisnik);
			return new ResponseEntity<Korisnik>(noviKorisnik, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Korisnik> updateArtikal(@RequestBody Korisnik korisnik){
		Korisnik k = korisnikService.findOne(korisnik.getKorisnickoIme());
		if (k == null) {
			return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
		} else {
			korisnikService.change(k);
			return new ResponseEntity<Korisnik>(korisnik, HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<Korisnik> deleteArtikal(@RequestBody Korisnik korisnik){
		if (korisnikService.findOne(korisnik.getKorisnickoIme()) == null) {
			return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
		} else {
			korisnikService.delete(korisnik);
			return new ResponseEntity<Korisnik>(HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "/{korisnickoIme}/{lozinka}", method = RequestMethod.GET)
	public ResponseEntity<Korisnik> loginKorisnik(@PathVariable("korisnickoIme") String korisnickoIme, 
													@PathVariable("lozinka") String lozinka){
		boolean prijava = korisnikService.login(korisnickoIme, lozinka);
		if (!prijava) {
			return new ResponseEntity<Korisnik>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<Korisnik>(HttpStatus.OK);
		}
	}

}
