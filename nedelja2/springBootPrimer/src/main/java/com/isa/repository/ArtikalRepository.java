package com.isa.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.isa.model.Artikal;


@Repository
public class ArtikalRepository {
	
	private ArrayList<Artikal> artikli;

	public ArtikalRepository() {
		super();
		artikli = new ArrayList<Artikal>();
		artikli.add(new Artikal(100.00, "maska za lice", 1));
		artikli.add(new Artikal(10000.00, "alkohol", 2));
		artikli.add(new Artikal(5000, "sredstvo za dezinfikovanje ruku", 3));
	}
	
	public List<Artikal> findAll(){
		return artikli;
	}
	
	public Artikal findOne(int id) {
		for (Artikal a : artikli) {
			if (a.getId() == id) {
				return a;
			}
		}
		
		return null;
		
	}
	
	public List<Artikal> priceRange(double start, double end){
		List<Artikal> pronadjeniArtikli = null;
		for (Artikal a: artikli) {
			if (a.getCena() >= start && a.getCena() <=end) {
				pronadjeniArtikli.add(a);
			}
		}
		return pronadjeniArtikli;
	}
	
	public String save(Artikal artikal) {
		Artikal a = findOne(artikal.getId());
		
		if (a == null) {
			artikli.add(a);
			return "Uspesno unet artikal.";
		} else {
			return "Artikal vec postoji.";
		}
	}
	
	public void change(Artikal artikal) {
		Artikal a = findOne(artikal.getId());
		if (a != null) {
			a.setCena(artikal.getCena());
			a.setOpis(artikal.getOpis());
		} else {
			System.out.println("Ne postoji ovaj artikal.");
		}
	}
	
	public void delete(int id) {
		Iterator<Artikal> iterator = artikli.iterator();
		while (iterator.hasNext()) {
			Artikal a = iterator.next();
			if (a.getId() == id) {
				iterator.remove();
				return;
			}
		}
	}
	
	public void delete(Artikal artikal) {
		delete(artikal.getId());
	}

}
