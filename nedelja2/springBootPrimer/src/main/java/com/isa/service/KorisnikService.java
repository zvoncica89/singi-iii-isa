package com.isa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isa.model.Korisnik;
import com.isa.repository.KorisnikRepository;


@Service
public class KorisnikService {
	@Autowired
	private KorisnikRepository korisnikRepository;
	
	public KorisnikService() {
	}
	
	public List<Korisnik> findAll(){
		return korisnikRepository.findAll();
	}
	
	public Korisnik findOne(String korisnickoIme) {
		return korisnikRepository.findOne(korisnickoIme);
	}
	
	public void save(Korisnik korisnik) {
		korisnikRepository.save(korisnik);
	}

	public void delete(Korisnik korisnik) {
		korisnikRepository.delete(korisnik);
	}
	
	public void change(Korisnik korisnik) {
		korisnikRepository.change(korisnik);
	}
	
	public boolean login(String korisnickoIme, String lozinka) {
		List<Korisnik> korisnici = korisnikRepository.findAll();
		for (Korisnik k: korisnici) {
			if (k.getKorisnickoIme().equals(korisnickoIme)) {
				return true;
			}
		}
		return false;
	}
}
