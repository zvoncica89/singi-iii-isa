package com.isa.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.isa.model.Artikal;
import com.isa.repository.ArtikalRepository;


@Service
public class ArtikalService {
	
	@Autowired
	private ArtikalRepository artikalRepository;
	
	public ArtikalService() {
		
	}
	
	public List<Artikal> findAll(){
		return artikalRepository.findAll();
	}
	
	public Artikal findOne(int id) {
		return artikalRepository.findOne(id);
	}

	public void save(Artikal artikal) {
		artikalRepository.save(artikal);
	}
	

	public void delete(Artikal artikal) {
		artikalRepository.delete(artikal);
	}

	public void change(Artikal artikal) {
		artikalRepository.change(artikal);
	}
	
	public List<Artikal> priceRange(double start, double end){
		return artikalRepository.priceRange(start, end);
	}
	
	public void sale(Artikal artikal, int procentage) {
		Artikal a = artikalRepository.findOne(artikal.getId());
		int saleProcentage = procentage/100;
		a.setCena(a.getCena()-(a.getCena()*saleProcentage));
	}

}
