package zadatak.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import zadatak.model.Artikal;
import zadatak.service.ArtikalService;

@Controller
@RequestMapping(path = "/api/artikli")
@CrossOrigin("http://localhost:8080")
public class ArtikalController {
	@Autowired
	private ArtikalService artikalService;
	
//	@RequestMapping(path = "", method = RequestMethod.GET)
//	public ResponseEntity<List<Artikal>> getAllArtikal(){
//		return new ResponseEntity<List<Artikal>>(artikalService.findAll(), HttpStatus.OK);
//	}
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<String> getAllArtikal(){
		return new ResponseEntity<String>("jej", HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idArtikla}", method = RequestMethod.GET)
	public ResponseEntity<Artikal> getOneArtikal(@PathVariable("idArtikla") int idArtikla){
		return new ResponseEntity<Artikal>(artikalService.findOne(idArtikla), HttpStatus.OK);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Artikal> createArtikal(@RequestBody Artikal noviArtikal){
		if (artikalService.findOne(noviArtikal.getId()) != null) {
			return new ResponseEntity<Artikal>(HttpStatus.CONFLICT);
		} else {
			artikalService.save(noviArtikal);
			return new ResponseEntity<Artikal>(noviArtikal, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> updateArtikal(@RequestBody Artikal artikal){
		Artikal a = artikalService.findOne(artikal.getId());
		if (a == null) {
			return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
		} else {
			artikalService.change(a);
			return new ResponseEntity<Artikal>(artikal, HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "", method = RequestMethod.DELETE)
	public ResponseEntity<Artikal> deleteArtikal(@RequestBody Artikal artikal){
		if (artikalService.findOne(artikal.getId()) == null) {
			return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
		} else {
			artikalService.delete(artikal);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
	}
	
	@RequestMapping(path = "/{idArtikla}/{procenat}", method = RequestMethod.PUT)
	public ResponseEntity<Artikal> saleArtikal(@PathVariable("idArtikla") int idArtikla,
												@PathVariable("procenat") int procenat){
		Artikal a = artikalService.findOne(idArtikla);
		if (a == null) {
			return new ResponseEntity<Artikal>(HttpStatus.NOT_FOUND);
		} else {
			artikalService.sale(a, procenat);
			return new ResponseEntity<Artikal>(HttpStatus.OK);
		}
	}

	
	

}
