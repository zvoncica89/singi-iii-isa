package zadatak.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class ArtikalLogger {
	
	@Around("@annotation(Logged)")
	public void logOkoArtikla(ProceedingJoinPoint jp) {
		System.out.println("Pocetak izvrsavanja: ");
		System.out.println(jp.getSignature());
		try {
			Object result = jp.proceed();
			System.out.println("Rezultat izvrsavanja: ");
			System.out.println(result);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		System.out.println("Kraj izvrsavanja: ");
		System.out.println(jp.getSignature());
		System.out.println("------------------");
	}

}
