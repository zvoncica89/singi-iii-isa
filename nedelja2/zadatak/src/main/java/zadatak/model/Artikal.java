package zadatak.model;

public class Artikal{
	private double cena;
	private String opis;
	private int id;
	
	
	
	public Artikal() {
		super();
	}

	public Artikal(double cena, String opis, int id) {
		super();
		this.cena = cena;
		this.opis = opis;
		this.id = id;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	

}
