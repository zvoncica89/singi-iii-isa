package zadatak.repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import zadatak.model.Korisnik;
import zadatak.model.Uloga;

@Repository
public class KorisnikRepository {
	private ArrayList<Korisnik> korisnici;	
	
	public KorisnikRepository() {
		super();
		korisnici = new ArrayList<Korisnik>();
		korisnici.add(new Korisnik("Mina", "Gusman", "mina", Uloga.administrator));
		korisnici.add(new Korisnik("Marko", "Zahorodni", "marko", Uloga.kupac));
	}

	public List<Korisnik> findAll(){
		return korisnici;
	}
	
	public Korisnik findOne(String korisnickoIme) {
		for (Korisnik k : korisnici) {
			if (k.getKorisnickoIme().equals(korisnickoIme)) {
				return k;
			}
		}
		
		return null;
		
	}
	
	public String save(Korisnik korisnik) {
		Korisnik k = findOne(korisnik.getKorisnickoIme());
		
		if (k == null) {
			korisnici.add(k);
			return "Uspesno unet korisnik.";
		} else {
			return "Korisnicko ime vec postoji u sistemu.";
		}
	}
	
	public void change(Korisnik korisnik) {
		Korisnik k = findOne(korisnik.getKorisnickoIme());
		if (k != null) {
			k.setIme(korisnik.getIme());
			k.setPrezime(korisnik.getPrezime());
			k.setUloga(korisnik.getUloga());
		} else {
			System.out.println("Ne postoji ovaj korisnik.");
		}
	}
	
	public void delete(String korisnickoIme) {
		Iterator<Korisnik> iterator = korisnici.iterator();
		while (iterator.hasNext()) {
			Korisnik k = iterator.next();
			if (k.getKorisnickoIme().equals(korisnickoIme)) {
				iterator.remove();
				return;
			}
		}
	}
	
	public void delete(Korisnik korisnik) {
		delete(korisnik.getKorisnickoIme());
	}
	

}
