package rs.mina.isa.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.mina.isa.dto.IznajmljivanjeDTO;
import rs.mina.isa.model.Iznajmljivanje;
import rs.mina.isa.service.IznajmljivanjeService;

@Controller
@RequestMapping(path = "/api/iznajmljivanja")
@CrossOrigin
public class IznajmljivanjeController {
	@Autowired
	private IznajmljivanjeService iznajmljivanjeService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<IznajmljivanjeDTO>> getAllIznajmljivanja(Pageable pageable){
		return new ResponseEntity<Page<IznajmljivanjeDTO>>(iznajmljivanjeService.findAll(pageable).map(i->new IznajmljivanjeDTO(i)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idIznajmljivanja}", method = RequestMethod.GET)
	public ResponseEntity<Iznajmljivanje> getIznajmljivanje(@PathVariable("idIznajmljivanja") Long idIznajmljivanja) {
		Iznajmljivanje iznajmljivanje= iznajmljivanjeService.findOne(idIznajmljivanja);
		if (iznajmljivanje != null) {
			return new ResponseEntity<Iznajmljivanje>(iznajmljivanje, HttpStatus.OK);
		}
		return new ResponseEntity<Iznajmljivanje>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Iznajmljivanje> createIznajmljivanje(@RequestBody Iznajmljivanje novoIznajmljivanje) {
		if (novoIznajmljivanje.getId() != null) {
		if (iznajmljivanjeService.findOne(novoIznajmljivanje.getId()) != null) {
			return new ResponseEntity<Iznajmljivanje>(HttpStatus.CONFLICT);
		}
		iznajmljivanjeService.save(novoIznajmljivanje);
		return new ResponseEntity<Iznajmljivanje>(novoIznajmljivanje, HttpStatus.CREATED);
		} else {
			iznajmljivanjeService.save(novoIznajmljivanje);
			return new ResponseEntity<Iznajmljivanje>(novoIznajmljivanje, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "/{idIznajmljivanja}", method = RequestMethod.PUT)
	public ResponseEntity<Iznajmljivanje> updateIznajmljivanje(@PathVariable("idIznajmljivanja") Long idIznajmljivanja,
			@RequestBody Iznajmljivanje podaciIznajmljivanje) {
		Iznajmljivanje iznajmljivanje = iznajmljivanjeService.findOne(idIznajmljivanja);
		if (iznajmljivanje == null) {
			return new ResponseEntity<Iznajmljivanje>(HttpStatus.NOT_FOUND);
		}
		iznajmljivanje.setDatumVracanja(podaciIznajmljivanje.getDatumVracanja());
		iznajmljivanjeService.save(iznajmljivanje);
		return new ResponseEntity<Iznajmljivanje>(iznajmljivanje, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idIznajmljivanja}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteIznajmljivanje(@PathVariable("idIznajmljivanja") Long idIznajmljivanja) {
		if (iznajmljivanjeService.findOne(idIznajmljivanja) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		iznajmljivanjeService.delete(idIznajmljivanja);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
