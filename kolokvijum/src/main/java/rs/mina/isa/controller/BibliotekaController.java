package rs.mina.isa.controller;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.mina.isa.dto.BibliotekaDTO;
import rs.mina.isa.model.Biblioteka;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.service.BibliotekaService;

@Controller
@RequestMapping(path = "/api/biblioteke")
@CrossOrigin
public class BibliotekaController {
	@Autowired
	private BibliotekaService bibliotekaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<BibliotekaDTO>> getAllBiblioteke(Pageable pageable){
		return new ResponseEntity<Page<BibliotekaDTO>>(bibliotekaService.findAll(pageable).map(b->new BibliotekaDTO(b)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idBiblioteke}", method = RequestMethod.GET)
	public ResponseEntity<Biblioteka> getBiblioteka(@PathVariable("idBiblioteke") Long idBiblioteke) {
		Biblioteka biblioteka= bibliotekaService.findOne(idBiblioteke);
		if (biblioteka != null) {
			return new ResponseEntity<Biblioteka>(biblioteka, HttpStatus.OK);
		}
		return new ResponseEntity<Biblioteka>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Biblioteka> createBiblioteka(@RequestBody Biblioteka novaBiblioteka) {
		if (novaBiblioteka.getId() != null) {
		if (bibliotekaService.findOne(novaBiblioteka.getId()) != null) {
			return new ResponseEntity<Biblioteka>(HttpStatus.CONFLICT);
		}
		bibliotekaService.save(novaBiblioteka);
		return new ResponseEntity<Biblioteka>(novaBiblioteka, HttpStatus.CREATED);
		} else {
			bibliotekaService.save(novaBiblioteka);
			return new ResponseEntity<Biblioteka>(novaBiblioteka, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "/{idBiblioteke}", method = RequestMethod.PUT)
	public ResponseEntity<Biblioteka> updateBiblioteka(@PathVariable("idBiblioteke") Long idBiblioteke,
			@RequestBody Biblioteka podaciBiblioteke) {
		Biblioteka biblioteka = bibliotekaService.findOne(idBiblioteke);
		if (biblioteka == null) {
			return new ResponseEntity<Biblioteka>(HttpStatus.NOT_FOUND);
		}
		biblioteka.setNaziv(podaciBiblioteke.getNaziv());
		biblioteka.setKnjige(podaciBiblioteke.getKnjige());
		bibliotekaService.save(biblioteka);
		return new ResponseEntity<Biblioteka>(podaciBiblioteke, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idBiblioteke}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteBiblioteka(@PathVariable("idBiblioteke") Long idBiblioteke) {
		if (bibliotekaService.findOne(idBiblioteke) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		bibliotekaService.delete(idBiblioteke);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{datum}", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<Knjiga>> getKnjigeByDate(@PathVariable("vreme") LocalDateTime datum) {
		ArrayList<Knjiga> potrebneKnjige = bibliotekaService.findAllBooksByDate(datum);
		if (potrebneKnjige != null) {
			return new ResponseEntity<ArrayList<Knjiga>>(potrebneKnjige, HttpStatus.OK);
		}
		return new ResponseEntity<ArrayList<Knjiga>>(HttpStatus.NOT_FOUND);
	}
	
	

}
