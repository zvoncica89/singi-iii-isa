package rs.mina.isa.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.mina.isa.dto.ClanDTO;
import rs.mina.isa.model.Clan;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.service.ClanService;

@Controller
@RequestMapping(path = "/api/clanovi")
@CrossOrigin
public class ClanController {
	@Autowired
	private ClanService clanService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<ClanDTO>> getAllClanovi(Pageable pageable){
		return new ResponseEntity<Page<ClanDTO>>(clanService.findAll(pageable).map(c->new ClanDTO(c)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idClana}", method = RequestMethod.GET)
	public ResponseEntity<Clan> getClan(@PathVariable("idClana") Long idClana) {
		Clan clan= clanService.findOne(idClana);
		if (clan != null) {
			return new ResponseEntity<Clan>(clan, HttpStatus.OK);
		}
		return new ResponseEntity<Clan>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Clan> createClan(@RequestBody Clan noviClan) {
		if (noviClan.getId() != null) {
		if (clanService.findOne(noviClan.getId()) != null) {
			return new ResponseEntity<Clan>(HttpStatus.CONFLICT);
		}
		clanService.save(noviClan);
		return new ResponseEntity<Clan>(noviClan, HttpStatus.CREATED);
		} else {
			clanService.save(noviClan);
			return new ResponseEntity<Clan>(noviClan, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "/{idClana}", method = RequestMethod.PUT)
	public ResponseEntity<Clan> updateClan(@PathVariable("idClana") Long idClana,
			@RequestBody Clan podaciClan) {
		Clan clan = clanService.findOne(idClana);
		if (clan == null) {
			return new ResponseEntity<Clan>(HttpStatus.NOT_FOUND);
		}
		clan.setIme(podaciClan.getIme());
		clan.setPrezime(podaciClan.getPrezime());
		clan.setIznajmljivanja(podaciClan.getIznajmljivanja());
		clanService.save(clan);
		return new ResponseEntity<Clan>(podaciClan, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idClana}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteClan(@PathVariable("idClana") Long idClana) {
		if (clanService.findOne(idClana) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		clanService.delete(idClana);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
	
	@RequestMapping(path = "/dobavi/{idClana}", method = RequestMethod.GET)
	public ResponseEntity<ArrayList<Knjiga>> getKnjigeByClan(@PathVariable("idClana") Long idClana) {
		ArrayList<Knjiga> potrebneKnjige= clanService.findAllNonReturnedBooks(idClana);
		if (idClana != null) {
			return new ResponseEntity<ArrayList<Knjiga>>(potrebneKnjige, HttpStatus.OK);
		}
		return new ResponseEntity<ArrayList<Knjiga>>(HttpStatus.NOT_FOUND);
	}
}
