package rs.mina.isa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import rs.mina.isa.dto.KnjigaDTO;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.service.KnjigaService;

@Controller
@RequestMapping(path = "/api/knjige")
@CrossOrigin
public class KnjigaController {
	@Autowired
	private KnjigaService knjigaService;
	
	@RequestMapping(path = "", method = RequestMethod.GET)
	public ResponseEntity<Page<KnjigaDTO>> getAllKnjige(Pageable pageable){
		return new ResponseEntity<Page<KnjigaDTO>>(knjigaService.findAll(pageable).map(k->new KnjigaDTO(k)), HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idKnjige}", method = RequestMethod.GET)
	public ResponseEntity<Knjiga> getKnjiga(@PathVariable("idKnjige") Long idKnjige) {
		Knjiga knjiga= knjigaService.findOne(idKnjige);
		if (knjiga != null) {
			return new ResponseEntity<Knjiga>(knjiga, HttpStatus.OK);
		}
		return new ResponseEntity<Knjiga>(HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<Knjiga> createKnjiga(@RequestBody Knjiga novaKnjiga) {
		if (novaKnjiga.getId() != null) {
		if (knjigaService.findOne(novaKnjiga.getId()) != null) {
			return new ResponseEntity<Knjiga>(HttpStatus.CONFLICT);
		}
		knjigaService.save(novaKnjiga);
		return new ResponseEntity<Knjiga>(novaKnjiga, HttpStatus.CREATED);
		} else {
			knjigaService.save(novaKnjiga);
			return new ResponseEntity<Knjiga>(novaKnjiga, HttpStatus.CREATED);
		}
	}
	
	@RequestMapping(path = "/{idKnjige}", method = RequestMethod.PUT)
	public ResponseEntity<Knjiga> updateKnjiga(@PathVariable("idKnjige") Long idKnjige,
			@RequestBody Knjiga podaciKnjige) {
		Knjiga knjiga = knjigaService.findOne(idKnjige);
		if (knjiga == null) {
			return new ResponseEntity<Knjiga>(HttpStatus.NOT_FOUND);
		}
		knjiga.setIsbn(podaciKnjige.getIsbn());
		knjiga.setNaslov(podaciKnjige.getNaslov());
		knjiga.setAutori(podaciKnjige.getAutori());
		knjiga.setIznajmljivanja(podaciKnjige.getIznajmljivanja());
		knjigaService.save(knjiga);
		return new ResponseEntity<Knjiga>(podaciKnjige, HttpStatus.OK);
	}
	
	@RequestMapping(path = "/{idKnjige}", method = RequestMethod.DELETE)
	public ResponseEntity<?> deleteKnjiga(@PathVariable("idKnjige") Long idKnjige) {
		if (knjigaService.findOne(idKnjige) == null) {
			return new ResponseEntity<Object>(HttpStatus.NOT_FOUND);
		}
		knjigaService.delete(idKnjige);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
