package rs.mina.isa.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.mina.isa.model.Clan;

@Repository
public interface ClanRepository extends PagingAndSortingRepository<Clan, Long>{

}
