package rs.mina.isa.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.mina.isa.model.Knjiga;

@Repository
public interface KnjigaRepository extends PagingAndSortingRepository<Knjiga, Long>{

}
