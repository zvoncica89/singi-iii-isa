package rs.mina.isa.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.mina.isa.model.Biblioteka;

@Repository
public interface BibliotekaRepository extends PagingAndSortingRepository<Biblioteka, Long>{

}
