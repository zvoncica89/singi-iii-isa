package rs.mina.isa.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import rs.mina.isa.model.Iznajmljivanje;

@Repository
public interface IznajmljivanjeRepository extends PagingAndSortingRepository<Iznajmljivanje, Long> {

}
