package rs.mina.isa.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.mina.isa.model.Clan;
import rs.mina.isa.model.Iznajmljivanje;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.repository.ClanRepository;
import rs.mina.isa.repository.IznajmljivanjeRepository;
import rs.mina.isa.repository.KnjigaRepository;

@Service
public class ClanService {
	@Autowired
	private ClanRepository clanRepository;
	@Autowired
	private KnjigaRepository knjigaRepository;
	@Autowired IznajmljivanjeRepository iznajmljivanjeRepository;

	public ClanService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Page<Clan> findAll(Pageable pageable){
		return clanRepository.findAll(pageable);
	}
	
	public Clan findOne(Long id) {
		return clanRepository.findById(id).orElse(null);
	}
	
	public void save(Clan clan) {
		clanRepository.save(clan);
	}
	
	public void delete(Long idClana) {
		clanRepository.deleteById(idClana);
	}
	
	public void delete (Clan clan) {
		clanRepository.delete(clan);
	}
	
	public ArrayList<Knjiga> findAllNonReturnedBooks(Long clanId){
		Iterable<Iznajmljivanje> svaIznajmljivanja = iznajmljivanjeRepository.findAll();
		ArrayList<Knjiga> potrebneKnjige = new ArrayList<Knjiga>();
		for (Iznajmljivanje i : svaIznajmljivanja) {
			if (i.getDatumVracanja() == null && i.getClan().getId().equals(clanId)) {
				potrebneKnjige.add(i.getKnjiga());
			}
		}
		return potrebneKnjige;		
	};

}
