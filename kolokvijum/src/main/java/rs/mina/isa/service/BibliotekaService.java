package rs.mina.isa.service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.mina.isa.model.Biblioteka;
import rs.mina.isa.model.Iznajmljivanje;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.repository.BibliotekaRepository;
import rs.mina.isa.repository.KnjigaRepository;

@Service
public class BibliotekaService {
	@Autowired
	BibliotekaRepository bibliotekaRepository;
	@Autowired
	KnjigaRepository knjigaRepository;

	public BibliotekaService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Page<Biblioteka> findAll(Pageable pageable){
		return bibliotekaRepository.findAll(pageable);
	}
	
	public Biblioteka findOne(Long id) {
		return bibliotekaRepository.findById(id).orElse(null);
	}
	
	public void save(Biblioteka biblioteka) {
		bibliotekaRepository.save(biblioteka);
	}
	
	public void delete(Long idBiblioteke) {
		bibliotekaRepository.deleteById(idBiblioteke);
	}
	
	public void delete (Biblioteka biblioteka) {
		bibliotekaRepository.delete(biblioteka);
	}
	
	public ArrayList<Knjiga> findAllBooksByDate(LocalDateTime datum){
		Iterable<Knjiga> sveKnjige = knjigaRepository.findAll();
		ArrayList<Knjiga> potrebneKnjige = new ArrayList<Knjiga>();
		for (Knjiga k : sveKnjige) {
			for (Iznajmljivanje i : k.getIznajmljivanja()) {
				if (datum.compareTo(i.getDatumIznajmljivanja()) < 0) {
					potrebneKnjige.add(k);
				}
			}
		}
		return potrebneKnjige;
		
	}

}
