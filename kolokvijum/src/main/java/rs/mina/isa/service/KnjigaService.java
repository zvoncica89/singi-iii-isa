package rs.mina.isa.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.mina.isa.model.Clan;
import rs.mina.isa.model.Knjiga;
import rs.mina.isa.repository.KnjigaRepository;

@Service
public class KnjigaService {
	@Autowired
	KnjigaRepository knjigaRepository;

	public KnjigaService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Iterable<Knjiga> findAll(){
		return knjigaRepository.findAll();
	}
	
	public Page<Knjiga> findAll(Pageable pageable){
		return knjigaRepository.findAll(pageable);
	}
	
	public Knjiga findOne(Long id) {
		return knjigaRepository.findById(id).orElse(null);
	}
	
	public void save(Knjiga knjiga) {
		knjigaRepository.save(knjiga);
	}
	
	public void delete(Long idKnjige) {
		knjigaRepository.deleteById(idKnjige);
	}
	
	public void delete (Knjiga knjiga) {
		knjigaRepository.delete(knjiga);
	}
}
