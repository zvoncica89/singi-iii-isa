package rs.mina.isa.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import rs.mina.isa.model.Clan;
import rs.mina.isa.model.Iznajmljivanje;
import rs.mina.isa.repository.IznajmljivanjeRepository;

@Service
public class IznajmljivanjeService {
	@Autowired
	IznajmljivanjeRepository iznajmljivanjeRepository;

	public IznajmljivanjeService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Iterable<?> findAll(){
		return iznajmljivanjeRepository.findAll();
	}
	
	public Page<Iznajmljivanje> findAll(Pageable pageable){
		return iznajmljivanjeRepository.findAll(pageable);
	}
	
	public Iznajmljivanje findOne(Long id) {
		return iznajmljivanjeRepository.findById(id).orElse(null);
	}
	
	public void save(Iznajmljivanje iznajmljivanje) {
		iznajmljivanjeRepository.save(iznajmljivanje);
	}
	
	public void delete(Long idIznajmljivanja) {
		iznajmljivanjeRepository.deleteById(idIznajmljivanja);
	}
	
	public void delete (Iznajmljivanje iznajmljivanje) {
		iznajmljivanjeRepository.delete(iznajmljivanje);
	}
	
	

}
