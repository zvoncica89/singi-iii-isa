package rs.mina.isa.dto;

import java.util.ArrayList;
import java.util.List;

import rs.mina.isa.model.Clan;
import rs.mina.isa.model.Iznajmljivanje;

public class ClanDTO {
	private Long id;
	private String ime;
	private String prezime;
	
	private List<IznajmljivanjeDTO> iznajmljivanja = new ArrayList<IznajmljivanjeDTO>();

	public ClanDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ClanDTO(Clan clan) {
		super();
		this.id = clan.getId();
		this.ime = clan.getIme();
		this.prezime = clan.getPrezime();
		for (Iznajmljivanje i : clan.getIznajmljivanja()) {
			iznajmljivanja.add(new IznajmljivanjeDTO(i.getId(), i.getDatumIznajmljivanja()));
		}
	}

	public ClanDTO(Long id, String ime, String prezime, List<IznajmljivanjeDTO> iznajmljivanja) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.iznajmljivanja = iznajmljivanja;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public List<IznajmljivanjeDTO> getIznajmljivanja() {
		return iznajmljivanja;
	}

	public void setIznajmljivanja(List<IznajmljivanjeDTO> iznajmljivanja) {
		this.iznajmljivanja = iznajmljivanja;
	}
	
	
	
	
	

}
