package rs.mina.isa.dto;

import java.util.ArrayList;
import java.util.List;

import rs.mina.isa.model.Iznajmljivanje;
import rs.mina.isa.model.Knjiga;

public class KnjigaDTO {
	private Long id;
	private String isbn;
	private String naslov;
	private String autori;
	
	private List<IznajmljivanjeDTO> iznajmljivanja = new ArrayList<IznajmljivanjeDTO>();

	public KnjigaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public KnjigaDTO(Knjiga knjiga) {
		super();
		this.id = knjiga.getId();
		this.isbn = knjiga.getIsbn();
		this.naslov = knjiga.getNaslov();
		this.autori = knjiga.getAutori();
		for (Iznajmljivanje i : knjiga.getIznajmljivanja()) {
			iznajmljivanja.add(new IznajmljivanjeDTO(i.getId(), i.getDatumIznajmljivanja()));
		}
	}

	public KnjigaDTO(Long id, String isbn, String naslov, String autori) {
		super();
		this.id = id;
		this.isbn = isbn;
		this.naslov = naslov;
		this.autori = autori;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getAutori() {
		return autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public List<IznajmljivanjeDTO> getIznajmljivanja() {
		return iznajmljivanja;
	}

	public void setIznajmljivanja(List<IznajmljivanjeDTO> iznajmljivanja) {
		this.iznajmljivanja = iznajmljivanja;
	}
	
	
	
	

}
