package rs.mina.isa.dto;

import java.time.LocalDateTime;

import rs.mina.isa.model.Iznajmljivanje;

public class IznajmljivanjeDTO {
	private Long id;
	private LocalDateTime datumIznajmljivanja;
	private LocalDateTime datumVracanja;
	
	
	public IznajmljivanjeDTO() {
		super();
		// TODO Auto-generated constructor stub
	}


	public IznajmljivanjeDTO(Iznajmljivanje iznajmljivanje) {
		super();
		this.id = iznajmljivanje.getId();
		this.datumIznajmljivanja = iznajmljivanje.getDatumIznajmljivanja();
		this.datumVracanja = iznajmljivanje.getDatumVracanja();
	}


	public IznajmljivanjeDTO(Long id, LocalDateTime datumIznajmljivanja) {
		super();
		this.id = id;
		this.datumIznajmljivanja = datumIznajmljivanja;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public LocalDateTime getDatumIznajmljivanja() {
		return datumIznajmljivanja;
	}


	public void setDatumIznajmljivanja(LocalDateTime datumIznajmljivanja) {
		this.datumIznajmljivanja = datumIznajmljivanja;
	}


	public LocalDateTime getDatumVracanja() {
		return datumVracanja;
	}


	public void setDatumVracanja(LocalDateTime datumVracanja) {
		this.datumVracanja = datumVracanja;
	}
	
	
	
	
	
	

}
