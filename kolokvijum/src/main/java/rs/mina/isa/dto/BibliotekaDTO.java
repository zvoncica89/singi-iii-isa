package rs.mina.isa.dto;

import java.util.ArrayList;
import java.util.List;

import rs.mina.isa.model.Biblioteka;
import rs.mina.isa.model.Knjiga;

public class BibliotekaDTO {
	private Long id;
	private String naziv;
	
	private List<KnjigaDTO> knjige = new ArrayList<KnjigaDTO>();

	public BibliotekaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BibliotekaDTO(Biblioteka biblioteka) {
		super();
		this.id = biblioteka.getId();
		this.naziv = biblioteka.getNaziv();
		for(Knjiga b : biblioteka.getKnjige()) {
			knjige.add(new KnjigaDTO(b.getId(), b.getIsbn(), b.getNaslov(), b.getAutori()));
		}
	}

	public BibliotekaDTO(Long id, String naziv, List<KnjigaDTO> knjige) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.knjige = knjige;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public List<KnjigaDTO> getKnjige() {
		return knjige;
	}

	public void setKnjige(List<KnjigaDTO> knjige) {
		this.knjige = knjige;
	}
	
	
	
	

}
