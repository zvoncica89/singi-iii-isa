package rs.mina.isa.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class Knjiga {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true, length = 64)
	private String isbn;
	
	@Column(nullable = false)
	@Type(type = "text")
	private String naslov;
	
	@Column(nullable = false)
	@Type(type = "text")
	private String autori;
	
	@ManyToOne
	@JoinColumn(name = "biblioteka_id", nullable = true)
	private Biblioteka biblioteka;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "knjiga")
	private List<Iznajmljivanje> iznajmljivanja;

	public Knjiga() {
		super();
		// TODO Auto-generated constructor stub
		this.iznajmljivanja = new ArrayList<Iznajmljivanje>();
	}

	public Knjiga(String isbn, String naslov, String autori) {
		super();
		this.isbn = isbn;
		this.naslov = naslov;
		this.autori = autori;
		this.iznajmljivanja = new ArrayList<Iznajmljivanje>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNaslov() {
		return naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getAutori() {
		return autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public Biblioteka getBiblioteka() {
		return biblioteka;
	}

	public void setBiblioteka(Biblioteka biblioteka) {
		this.biblioteka = biblioteka;
	}

	public List<Iznajmljivanje> getIznajmljivanja() {
		return iznajmljivanja;
	}

	public void setIznajmljivanja(List<Iznajmljivanje> iznajmljivanja) {
		this.iznajmljivanja = iznajmljivanja;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((autori == null) ? 0 : autori.hashCode());
		result = prime * result + ((biblioteka == null) ? 0 : biblioteka.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isbn == null) ? 0 : isbn.hashCode());
		result = prime * result + ((iznajmljivanja == null) ? 0 : iznajmljivanja.hashCode());
		result = prime * result + ((naslov == null) ? 0 : naslov.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Knjiga other = (Knjiga) obj;
		if (autori == null) {
			if (other.autori != null)
				return false;
		} else if (!autori.equals(other.autori))
			return false;
		if (biblioteka == null) {
			if (other.biblioteka != null)
				return false;
		} else if (!biblioteka.equals(other.biblioteka))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isbn == null) {
			if (other.isbn != null)
				return false;
		} else if (!isbn.equals(other.isbn))
			return false;
		if (iznajmljivanja == null) {
			if (other.iznajmljivanja != null)
				return false;
		} else if (!iznajmljivanja.equals(other.iznajmljivanja))
			return false;
		if (naslov == null) {
			if (other.naslov != null)
				return false;
		} else if (!naslov.equals(other.naslov))
			return false;
		return true;
	}

	
	
	
	
	
	

}
