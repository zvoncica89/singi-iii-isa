package rs.mina.isa.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
@Inheritance(strategy = InheritanceType.JOINED)
public class Iznajmljivanje {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false, unique = true)
	private LocalDateTime datumIznajmljivanja;
	
	@Column(nullable = false, unique = true)
	private LocalDateTime datumVracanja;
	
	@ManyToOne
	@JoinColumn(name = "knjiga_id", nullable = true)
	private Knjiga knjiga;
	
	@ManyToOne
	@JoinColumn(name = "clan_id", nullable = true)
	private Clan clan;

	public Iznajmljivanje() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Iznajmljivanje(LocalDateTime datumIznajmljivanja, Knjiga knjiga,	Clan clan) {
		super();
		this.datumIznajmljivanja = datumIznajmljivanja;
		this.knjiga = knjiga;
		this.clan = clan;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getDatumIznajmljivanja() {
		return datumIznajmljivanja;
	}

	public void setDatumIznajmljivanja(LocalDateTime datumIznajmljivanja) {
		this.datumIznajmljivanja = datumIznajmljivanja;
	}

	public LocalDateTime getDatumVracanja() {
		return datumVracanja;
	}

	public void setDatumVracanja(LocalDateTime datumVracanja) {
		this.datumVracanja = datumVracanja;
	}

	public Knjiga getKnjiga() {
		return knjiga;
	}

	public void setKnjiga(Knjiga knjiga) {
		this.knjiga = knjiga;
	}

	public Clan getClan() {
		return clan;
	}

	public void setClan(Clan clan) {
		this.clan = clan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((clan == null) ? 0 : clan.hashCode());
		result = prime * result + ((datumIznajmljivanja == null) ? 0 : datumIznajmljivanja.hashCode());
		result = prime * result + ((datumVracanja == null) ? 0 : datumVracanja.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((knjiga == null) ? 0 : knjiga.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Iznajmljivanje other = (Iznajmljivanje) obj;
		if (clan == null) {
			if (other.clan != null)
				return false;
		} else if (!clan.equals(other.clan))
			return false;
		if (datumIznajmljivanja == null) {
			if (other.datumIznajmljivanja != null)
				return false;
		} else if (!datumIznajmljivanja.equals(other.datumIznajmljivanja))
			return false;
		if (datumVracanja == null) {
			if (other.datumVracanja != null)
				return false;
		} else if (!datumVracanja.equals(other.datumVracanja))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (knjiga == null) {
			if (other.knjiga != null)
				return false;
		} else if (!knjiga.equals(other.knjiga))
			return false;
		return true;
	}

	
	
	

}
