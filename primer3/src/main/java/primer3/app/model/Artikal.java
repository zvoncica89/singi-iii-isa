package primer3.app.model;

public class Artikal {
	private String naziv;
	private Double cena;
	private String opis;
	private int id;	
	
	public Artikal() {
		super();
	}

	public Artikal(String naziv, Double cena, String opis, int id) {
		super();
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
	
	

}
