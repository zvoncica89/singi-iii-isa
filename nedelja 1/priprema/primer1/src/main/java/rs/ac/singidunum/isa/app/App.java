package rs.ac.singidunum.isa.app;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import rs.ac.singidunum.isa.app.view.RacunView;

public class App {
	public static void main(String[] args) {
		try(ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("beans.xml")) {
			RacunView view = context.getBean(RacunView.class);
			view.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
