package rs.ac.singidunum.isa.app;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import rs.ac.singidunum.isa.app.configuration.AppConfiguration;
import rs.ac.singidunum.isa.app.view.RacunView;

public class App {
	public static void main(String[] args) {
		try(AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppConfiguration.class)) {
			RacunView view = context.getBean(RacunView.class);
			view.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
	}
}
