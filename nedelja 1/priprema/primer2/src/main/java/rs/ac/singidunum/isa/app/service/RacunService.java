package rs.ac.singidunum.isa.app.service;

import java.util.List;

import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.repository.RacunRepository;

public class RacunService {
	private RacunRepository racunRepository;

	public RacunService() {
	}
	
	public RacunService(RacunRepository racunRepository) {
		super();
		this.racunRepository = racunRepository;
	}

	public RacunRepository getRacunRepository() {
		return racunRepository;
	}

	public void setRacunRepository(RacunRepository racunRepository) {
		this.racunRepository = racunRepository;
	}
	
	public List<Racun> findAll() {
		return racunRepository.findAll();
	}
	
	public Racun findOne(String brojRacuna) {
		return racunRepository.findOne(brojRacuna);
	}
	
	public void save(Racun racun) {
		racunRepository.save(racun);
	}
	
	public void delete(String brojRacuna) {
		racunRepository.delete(brojRacuna);
	}
	
	public void delete(Racun racun) {
		racunRepository.delete(racun);
	}
	
	public void prenos(String brojRacunaUplatioca, String brojRacunaPrimaoca, Double iznos) {
		Racun uplatilac = racunRepository.findOne(brojRacunaUplatioca);
		Racun primalac = racunRepository.findOne(brojRacunaPrimaoca);
		uplatilac.setStajne(uplatilac.getStajne()-iznos);
		primalac.setStajne(primalac.getStajne()+iznos);
		racunRepository.save(uplatilac);
		racunRepository.save(primalac);
	}
}
