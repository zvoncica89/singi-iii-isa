package rs.ac.singidunum.isa.app.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import rs.ac.singidunum.isa.app.model.Racun;
import rs.ac.singidunum.isa.app.service.RacunService;

public class RacunView {

	private RacunService racunService;

	public RacunView() {
		super();
	}

	public RacunView(RacunService racunService) {
		super();
		this.racunService = racunService;
	}

	public RacunService getRacunService() {
		return racunService;
	}

	public void setRacunService(RacunService racunService) {
		this.racunService = racunService;
	}

	public void show() {
		int izbor = 0;
		do {
			glavniMeni();
			try {
				izbor = Integer.parseInt(ocitajUlaz());
			} catch (Exception e) {
				izbor = -1;
			}

			switch (izbor) {
			case 1:
				prikazSvih();
				break;
			case 2:
				prikazPoBrojuRacuna();
				break;
			case 3:
				dodavanjeRacuna();
				break;
			case 4:
				izmenaIznosa();
				break;
			case 5:
				uklanjanje();
				break;
			case 6:
				prenos();
				break;
			}
		} while (izbor != 0);

	}

	protected void glavniMeni() {
		System.out.println("Izaberite opciju:");
		System.out.println("1. Prikaz svih racuna");
		System.out.println("2. Prikaz racuna po broju racuna");
		System.out.println("3. Dodavanje racuna");
		System.out.println("4. Izmena iznosa na racunu");
		System.out.println("5. Uklanjanje racuna");
		System.out.println("6. Prenos sredstava");
		System.out.println("0. Izlaz");
	}

	protected String ocitajUlaz() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		try {
			return reader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected void prikazSvih() {
		List<Racun> racuni = racunService.findAll();
		for (Racun r : racuni) {
			System.out.println(r.getBrojRacuna() + "\t" + r.getStajne());
		}
	}

	protected void prikazPoBrojuRacuna() {
		System.out.println("Unesite broj racuna: ");
		Racun r = racunService.findOne(ocitajUlaz());
		if (r != null) {
			System.out.println(r.getBrojRacuna() + "\t" + r.getStajne());
		} else {
			System.out.println("Racun nije pronadjen.");
		}
	}

	protected void dodavanjeRacuna() {
		System.out.println("Unesite broj racuna: ");
		String brojRacuna = ocitajUlaz();
		if (racunService.findOne(brojRacuna) != null) {
			System.out.println("Ovaj broj racun vec postoji");
			return;
		}
		System.out.println("Unesite pocetni iznos: ");
		Double iznos = Double.parseDouble(ocitajUlaz());
		racunService.save(new Racun(brojRacuna, iznos));
	}

	protected void izmenaIznosa() {
		Racun r;
		System.out.println("Unesite broj racuna: ");
		String brojRacuna = ocitajUlaz();
		if ((r = racunService.findOne(brojRacuna)) == null) {
			System.out.println("Racun sa zadatim brojem racuna nije pronadjen!");
			return;
		}
		System.out.println("Unesite novi iznos: ");
		Double iznos = Double.parseDouble(ocitajUlaz());
		r.setStajne(iznos);
		racunService.save(r);
	}

	protected void uklanjanje() {
		System.out.println("Unesite broj racuna: ");
		String brojRacuna = ocitajUlaz();
		if (racunService.findOne(brojRacuna) == null) {
			System.out.println("Racun sa zadatim brojem racuna nije pronadjen!");
			return;
		}
		racunService.delete(brojRacuna);
	}

	protected void prenos() {
		System.out.println("Unesite broj racuna uplatioca: ");
		String brojRacunaUplatioca = ocitajUlaz();
		if (racunService.findOne(brojRacunaUplatioca) == null) {
			System.out.println("Racun uplatiocanije pronadjen!");
			return;
		}
		System.out.println("Unesite broj racuna primaoca: ");
		String brojRacunaPrimaoca = ocitajUlaz();
		if (racunService.findOne(brojRacunaPrimaoca) == null) {
			System.out.println("Racun primaoca nije pronadjen!");
			return;
		}
		System.out.println("Unesite novi iznos: ");
		Double iznos = Double.parseDouble(ocitajUlaz());
		racunService.prenos(brojRacunaUplatioca, brojRacunaPrimaoca, iznos);
	}
}
