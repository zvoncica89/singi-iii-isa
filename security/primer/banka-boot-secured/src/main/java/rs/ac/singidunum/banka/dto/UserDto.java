package rs.ac.singidunum.banka.dto;

import rs.ac.singidunum.banka.model.User;

public class UserDto {
	private String username;

	public UserDto() {
		super();
	}

	public UserDto(String username) {
		super();
		this.username = username;
	}
	
	public UserDto(User user) {
		this(user.getUsername());
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
