package rs.ac.singidunum.banka.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import rs.ac.singidunum.banka.model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	Optional<User> getByUsername(String username);
	Optional<User> getByUsernameAndPassword(String username, String password);
}
