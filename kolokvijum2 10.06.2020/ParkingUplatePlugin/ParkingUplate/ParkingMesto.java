package ParkingUplate;


@javax.persistence.Entity

@Table(name = "parking_mesto")
public class ParkingMesto {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "parking_mesto_id")
    private Long id;
        
    private String ulica;
        
    private String zona;
        
    @javax.persistence.OneToMany(mappedBy="parkingMesto")
    private java.util.List<ParkingKarta> parkingKarta;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getUlica () {
        return this.ulica;
    }

    public void setUlica (String ulica) {
        this.ulica = ulica;
    }
    
    public String getZona () {
        return this.zona;
    }

    public void setZona (String zona) {
        this.zona = zona;
    }
    
    public java.util.List<ParkingKarta> getParkingKarta () {
        return this.parkingKarta;
    }

    public void setParkingKarta (java.util.List<ParkingKarta> parkingKarta) {
        this.parkingKarta = parkingKarta;
    }
    


}