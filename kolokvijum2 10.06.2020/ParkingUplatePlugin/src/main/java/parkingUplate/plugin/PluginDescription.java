package parkingUplate.plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.HttpMethod;

/*
 * Klasa koja sluzi za prenos metapodataka o plaguin-u.
 */
public class PluginDescription {
    private String name;
    private List<String> categories;
    private String description;
    private String scheme; // scheme, host nam sluze da bismo izgradili URL preko kojeg se moze pristupiti
			   // plugin-u.
    private String host;

    // Spisak svih endpointa koje plugin daje na upotrebu plugin hostu.
    // Endpointi su razvrstani po tipu zahteva a potom po nazivu.
//    private HashMap<HttpMethod, HashMap<String, String>> endpoints = new HashMap<HttpMethod, HashMap<String, String>>();
    private HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints = new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>();

    public PluginDescription() {
    }

    public PluginDescription(String name, List<String> categories, String description, String scheme, String host,
	    HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints) {
	super();
	this.name = name;
	this.categories = categories;
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
    }

    public PluginDescription(String name, String description, String scheme, String host,
	    HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints) {
	super();
	this.name = name;
	this.categories = new ArrayList<String>();
	this.description = description;
	this.scheme = scheme;
	this.host = host;
	this.endpoints = endpoints;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public List<String> getCategories() {
	return categories;
    }

    public void setCategories(List<String> category) {
	this.categories = category;
    }

    public String getDescription() {
	return description;
    }

    public void setDescription(String description) {
	this.description = description;
    }

    public String getScheme() {
	return scheme;
    }

    public void setScheme(String scheme) {
	this.scheme = scheme;
    }

    public String getHost() {
	return host;
    }

    public void setHost(String host) {
	this.host = host;
    }

    public String getUrl() {
	return this.scheme + "://" + this.host;
    }

    public HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> getEndpoints() {
	return endpoints;
    }

    public void setEndpoints(HashMap<String, HashMap<HttpMethod, HashMap<String, String>>> endpoints) {
	this.endpoints = endpoints;
    }

    /*
     * Pomocna metoda za dobavljanje endopinta po tipu zahteva i nazivu endpointa.
     */
    public String getEndpointUrl(String category, HttpMethod method, String endpointName) {
	HashMap<String, String> existingEndpoitns = this.endpoints.get(category).get(method);
	if (existingEndpoitns != null) {
	    return existingEndpoitns.get(endpointName);
	}
	return null;
    }
}