package parkingUplate.model;

import javax.persistence.Column;
import javax.persistence.Table;

import parkingUplate.DTO.ParkingMestoDTO;

@javax.persistence.Entity

@Table(name = "parking_mesto")
public class ParkingMesto implements CreateDTO<Long>, Identitet{

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "parking_mesto_id")
    private Long id;
        
    private String ulica;
        
    private String zona;
        
    @javax.persistence.OneToMany(mappedBy="parkingMesto")
    private java.util.List<ParkingKartaUplata> parkingKarta;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getUlica () {
        return this.ulica;
    }

    public void setUlica (String ulica) {
        this.ulica = ulica;
    }
    
    public String getZona () {
        return this.zona;
    }

    public void setZona (String zona) {
        this.zona = zona;
    }
    
    public java.util.List<ParkingKartaUplata> getParkingKarta () {
        return this.parkingKarta;
    }

    public void setParkingKarta (java.util.List<ParkingKartaUplata> parkingKarta) {
        this.parkingKarta = parkingKarta;
    }

	@Override
	public ParkingMestoDTO getDTO(boolean isInsideDTO) {
		ParkingMestoDTO parkingMestoDTO = new ParkingMestoDTO(this, isInsideDTO);
		return parkingMestoDTO;
	}
    


}