package parkingUplate.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import parkingUplate.DTO.ParkingKartaUplataDTO;

@javax.persistence.Entity

@Table(name = "parking_karta")
public class ParkingKartaUplata implements CreateDTO<Long>, Identitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "parking_karta_id")
    private Long id;
        
    private String registracioniBroj;
        
    private LocalDateTime pocetakVazenja;
        
    private LocalDateTime krajVazenja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "parking_mesto_id")
    private ParkingMesto parkingMesto;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getRegistracioniBroj () {
        return this.registracioniBroj;
    }

    public void setRegistracioniBroj (String registracioniBroj) {
        this.registracioniBroj = registracioniBroj;
    }
    
    public LocalDateTime getPocetakVazenja () {
        return this.pocetakVazenja;
    }

    public void setPocetakVazenja (LocalDateTime pocetakVazenja) {
        this.pocetakVazenja = pocetakVazenja;
    }
    
    public LocalDateTime getKrajVazenja () {
        return this.krajVazenja;
    }

    public void setKrajVazenja (LocalDateTime krajVazenja) {
        this.krajVazenja = krajVazenja;
    }
    
    public ParkingMesto getParkingMesto () {
        return this.parkingMesto;
    }

    public void setParkingMesto (ParkingMesto parkingMesto) {
        this.parkingMesto = parkingMesto;
    }

	@Override
	public ParkingKartaUplataDTO getDTO(boolean isInsideDTO) {
		ParkingKartaUplataDTO parkingKartaDTO = new ParkingKartaUplataDTO(this, isInsideDTO);
		return parkingKartaDTO;
	}
    


}