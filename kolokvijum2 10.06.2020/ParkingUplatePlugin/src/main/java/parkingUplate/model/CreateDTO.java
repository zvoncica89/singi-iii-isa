package parkingUplate.model;

import parkingUplate.DTO.DTO;

public interface CreateDTO<ID> {
	public DTO getDTO(boolean isInsideDTO);
	public ID getId();

}
