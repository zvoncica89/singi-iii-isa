package parkingUplate.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import parkingUplate.model.ParkingMesto;

@Repository
public interface ParkingMestoRepository extends PagingAndSortingRepository<ParkingMesto, Long> {

}
