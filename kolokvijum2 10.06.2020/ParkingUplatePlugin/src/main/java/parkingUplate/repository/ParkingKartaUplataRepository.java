package parkingUplate.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import parkingUplate.model.ParkingKartaUplata;

@Repository
public interface ParkingKartaUplataRepository extends PagingAndSortingRepository<ParkingKartaUplata, Long> {

}
