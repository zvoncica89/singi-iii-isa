package parkingUplate;

import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

import parkingUplate.plugin.PluginDescription;


@SpringBootApplication
@EnableAutoConfiguration(exclude = { SecurityAutoConfiguration.class })
public class App {

	public static void main(String[] args) {
		RestTemplate rt = new RestTemplate();

		PluginDescription pluginDescription = new PluginDescription("ParkingUplate", "Plugin za logicku celinu parking uplate", "http",
				"localhost:8102", new HashMap<String, HashMap<HttpMethod, HashMap<String, String>>>());

		ArrayList<String> categories = new ArrayList<String>();
		categories.add("parkingKartaUplata");
		categories.add("parkingMesto");
		pluginDescription.setCategories(categories);

		for (String category : categories) {
			//			dodajemo spisak endpoint-a u opis plugin-a
			pluginDescription.getEndpoints().put(category, new HashMap<HttpMethod, HashMap<String, String>>());
			pluginDescription.getEndpoints().get(category).put(HttpMethod.GET, new HashMap<String, String>());
			pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findAll",
					"/api/" + category);
			pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findOne",
					"/api/" + category + "/");

			pluginDescription.getEndpoints().get(category).put(HttpMethod.POST, new HashMap<String, String>());
			pluginDescription.getEndpoints().get(category).get(HttpMethod.POST).putIfAbsent("create",
					"/api/" + category);

			pluginDescription.getEndpoints().get(category).put(HttpMethod.PUT, new HashMap<String, String>());
			pluginDescription.getEndpoints().get(category).get(HttpMethod.PUT).putIfAbsent("update",
					"/api/" + category);

			pluginDescription.getEndpoints().get(category).put(HttpMethod.DELETE, new HashMap<String, String>());
			pluginDescription.getEndpoints().get(category).get(HttpMethod.DELETE).putIfAbsent("delete",
					"/api/" + category + "/");
			//ovde dodajemo sve ono sto nisu uobicajene metode
//			if (category.equals("gost")){
//				pluginDescription.getEndpoints().get(category).get(HttpMethod.GET).putIfAbsent("findRezervacijaByGostId",
//						"/api/" + category + "/mojeRezervacije/");
//			}
		}
		//		registracija plugin-a na host-u
		rt.postForLocation("http://localhost:8080/api/plugins", pluginDescription);

		//		pokretanje plugin-a
		SpringApplication.run(App.class, args);

	}

}
