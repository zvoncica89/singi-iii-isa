package parkingUplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import parkingUplate.model.ParkingMesto;

@Controller
@RequestMapping(path = "/api/parkingMesto")
@CrossOrigin(origins = "*")
public class ParkingMestoController extends CRUDController<ParkingMesto, Long> {

}
