package parkingUplate.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import parkingUplate.model.ParkingKartaUplata;

@Controller
@RequestMapping(path = "/api/parkingKartaUplata")
@CrossOrigin(origins = "*")
public class ParkingKartaUplataController extends CRUDController<ParkingKartaUplata, Long> {

}
