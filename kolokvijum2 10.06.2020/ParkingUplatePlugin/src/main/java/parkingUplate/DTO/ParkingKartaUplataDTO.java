package parkingUplate.DTO;

import java.time.LocalDateTime;

import parkingUplate.model.ParkingKartaUplata;

public class ParkingKartaUplataDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String registracioniBroj;
	private LocalDateTime pocetakVazenja;
	private LocalDateTime krajVazenja;
	private ParkingMestoDTO parkingMesto;
	public ParkingKartaUplataDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ParkingKartaUplataDTO(Long id, String registracioniBroj, LocalDateTime pocetakVazenja, LocalDateTime krajVazenja,
			ParkingMestoDTO parkingMesto) {
		super();
		this.id = id;
		this.registracioniBroj = registracioniBroj;
		this.pocetakVazenja = pocetakVazenja;
		this.krajVazenja = krajVazenja;
		this.parkingMesto = parkingMesto;
	}
	
	public ParkingKartaUplataDTO(ParkingKartaUplata parkingKarta, boolean isInsideDto) {
		this.id = parkingKarta.getId();
		this.registracioniBroj = parkingKarta.getRegistracioniBroj();
		this.pocetakVazenja = parkingKarta.getPocetakVazenja();
		this.krajVazenja = parkingKarta.getKrajVazenja();
		if (!isInsideDto) {
			this.parkingMesto = parkingKarta.getParkingMesto().getDTO(!isInsideDto);
		}
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegistracioniBroj() {
		return registracioniBroj;
	}
	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}
	public LocalDateTime getPocetakVazenja() {
		return pocetakVazenja;
	}
	public void setPocetakVazenja(LocalDateTime pocetakVazenja) {
		this.pocetakVazenja = pocetakVazenja;
	}
	public LocalDateTime getKrajVazenja() {
		return krajVazenja;
	}
	public void setKrajVazenja(LocalDateTime krajVazenja) {
		this.krajVazenja = krajVazenja;
	}
	public ParkingMestoDTO getParkingMesto() {
		return parkingMesto;
	}
	public void setParkingMesto(ParkingMestoDTO parkingMesto) {
		this.parkingMesto = parkingMesto;
	}
	
	

}
