package parkingUplate.DTO;

import java.util.List;
import java.util.stream.Collectors;

import parkingUplate.model.ParkingMesto;

public class ParkingMestoDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String ulica;
	private String zona;
	private java.util.List<ParkingKartaUplataDTO> parkingKarta;

	public ParkingMestoDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ParkingMestoDTO(Long id, String ulica, String zona, List<ParkingKartaUplataDTO> parkingKarta) {
		super();
		this.id = id;
		this.ulica = ulica;
		this.zona = zona;
		this.parkingKarta = parkingKarta;
	}
	
	public ParkingMestoDTO(ParkingMesto parkingMesto, boolean isInsideDto) {
		this.id = parkingMesto.getId();
		this.ulica = parkingMesto.getUlica();
		this.zona = parkingMesto.getZona();
		if (!isInsideDto) {
			this.parkingKarta = parkingMesto.getParkingKarta().stream().map(p -> p.getDTO(!isInsideDto)).collect(Collectors.toList());;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUlica() {
		return ulica;
	}

	public void setUlica(String ulica) {
		this.ulica = ulica;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public java.util.List<ParkingKartaUplataDTO> getParkingKarta() {
		return parkingKarta;
	}

	public void setParkingKarta(java.util.List<ParkingKartaUplataDTO> parkingKarta) {
		this.parkingKarta = parkingKarta;
	}

	
}
