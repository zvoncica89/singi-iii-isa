package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Rezervacija;

@Controller
@RequestMapping(path = "/api/rezervacija" )
public class RezervacijaController extends HostCrudController<Rezervacija, Long> {
	
	public RezervacijaController() {
		this.category = "rezervacija";
	}

}
