package pluginHost.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import pluginHost.entity.Soba;

@Controller
@RequestMapping(path = "/api/soba" )
public class SobaController extends HostCrudController<Soba, Long> {

	public SobaController() {
		this.category = "soba";
	}
}
