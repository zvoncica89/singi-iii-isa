package pluginHost.controller;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pluginHost.entity.Gost;
import puginHost.model.Plugin;

@Controller
@RequestMapping(path = "/api/gost" )
public class GostController extends HostCrudController<Gost, Long> {
	
	public GostController() {
		this.category = "gost";
	}
	
	 @RequestMapping(path = "/mojeRezervacije/{id}", method = RequestMethod.GET)
	    public ResponseEntity<?> findRezervacijaByGostId(@PathVariable ("id") Long id, Pageable pageable, @RequestHeader HttpHeaders httpHeaders) {
		// http://localhost:8080/api/adresa?page=0&size=2&sort=tip,neki_drugi_property,desc&sort=id

		/*
		 * Zahtev za dobavljanje svih adresa delegira se prvom registrovanom pluginu u
		 * kategoriji address. Ovom pluginu se upucuje get zahtev na endpoint pod
		 * nazivom getAll.
		 */
		Plugin plugin = pr.getPlugins(this.category).get(0);
		return plugin.findAllBy(this.category, HttpMethod.GET, "findRezervacijaByGostId", id, pageable, httpHeaders);
	    }
	
}
