package pluginHost.entity;

import java.time.LocalDateTime;



public class Rezervacija implements Identitet {

	private Long id;
	
	private LocalDateTime odKad;
	
	private LocalDateTime doKad;
	
	private Gost gost;
	
	private Soba soba;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LocalDateTime getOdKad() {
		return odKad;
	}

	public void setOdKad(LocalDateTime odKad) {
		this.odKad = odKad;
	}

	public LocalDateTime getDoKad() {
		return doKad;
	}

	public void setDoKad(LocalDateTime doKad) {
		this.doKad = doKad;
	}

	public Gost getGost() {
		return gost;
	}

	public void setGost(Gost gost) {
		this.gost = gost;
	}

	public Soba getSoba() {
		return soba;
	}

	public void setSoba(Soba soba) {
		this.soba = soba;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((doKad == null) ? 0 : doKad.hashCode());
		result = prime * result + ((gost == null) ? 0 : gost.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((odKad == null) ? 0 : odKad.hashCode());
		result = prime * result + ((soba == null) ? 0 : soba.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rezervacija other = (Rezervacija) obj;
		if (doKad == null) {
			if (other.doKad != null)
				return false;
		} else if (!doKad.equals(other.doKad))
			return false;
		if (gost == null) {
			if (other.gost != null)
				return false;
		} else if (!gost.equals(other.gost))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (odKad == null) {
			if (other.odKad != null)
				return false;
		} else if (!odKad.equals(other.odKad))
			return false;
		if (soba == null) {
			if (other.soba != null)
				return false;
		} else if (!soba.equals(other.soba))
			return false;
		return true;
	}

	public Rezervacija() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Rezervacija(Long id, LocalDateTime odKad, LocalDateTime doKad, Gost gost, Soba soba) {
		super();
		this.id = id;
		this.odKad = odKad;
		this.doKad = doKad;
		this.gost = gost;
		this.soba = soba;
	}
	
	
	

	
	
	

}
