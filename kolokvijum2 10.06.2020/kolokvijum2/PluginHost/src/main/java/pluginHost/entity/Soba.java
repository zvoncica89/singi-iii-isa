package pluginHost.entity;

import java.util.List;



public class Soba implements Identitet {
	private Long id;
	
	private String naziv;
	
	private String opis;
	private Long brojKreveta;

	private List<Rezervacija> rezervacija;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public Long getBrojKreveta() {
		return brojKreveta;
	}

	public void setBrojKreveta(Long brojKreveta) {
		this.brojKreveta = brojKreveta;
	}

	public List<Rezervacija> getRezervacija() {
		return rezervacija;
	}

	public void setRezervacija(List<Rezervacija> rezervacija) {
		this.rezervacija = rezervacija;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brojKreveta == null) ? 0 : brojKreveta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((rezervacija == null) ? 0 : rezervacija.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Soba other = (Soba) obj;
		if (brojKreveta == null) {
			if (other.brojKreveta != null)
				return false;
		} else if (!brojKreveta.equals(other.brojKreveta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (rezervacija == null) {
			if (other.rezervacija != null)
				return false;
		} else if (!rezervacija.equals(other.rezervacija))
			return false;
		return true;
	}

	public Soba() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Soba(Long id, String naziv, String opis, Long brojKreveta, List<Rezervacija> rezervacija) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.opis = opis;
		this.brojKreveta = brojKreveta;
		this.rezervacija = rezervacija;
	}
	
	
	
	

}
