package com.kolokvijum.hotel.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.repository.GostRepository;


@Service
public class UserService {
	@Autowired
	GostRepository gostRepository;
	
	public Optional<Gost> getUser(String ime) {
		return gostRepository.getByIme(ime);
	}
	
	public Optional<Gost> getUser(String ime, String lozinka) {
		return gostRepository.getByImeAndLozinka(ime, lozinka);
	}
}
