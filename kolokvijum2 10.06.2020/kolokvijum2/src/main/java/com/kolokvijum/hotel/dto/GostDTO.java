package com.kolokvijum.hotel.dto;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.model.Rezervacija;

public class GostDTO {
	private String ime;
	private String prezime;
	private String email;
	private Rezervacija rezervacija;
	
	public GostDTO() {
		super();
	}
	
	public GostDTO(String ime, String prezime, String email, Rezervacija rezervacija) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.email = email;
		this.rezervacija = rezervacija;
	}
	
	public GostDTO (Gost gost) {
		super();
		this.ime = gost.getIme();
		this.prezime = gost.getPrezime();
		this.email = gost.getEmail();
		this.rezervacija = (Rezervacija) gost.getRezervacija();
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Rezervacija getRezervacija() {
		return rezervacija;
	}

	public void setRezervacija(Rezervacija rezervacija) {
		this.rezervacija = rezervacija;
	}
	
	
	
	
}
