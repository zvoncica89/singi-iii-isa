package com.kolokvijum.hotel.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kolokvijum.hotel.model.Gost;
import com.kolokvijum.hotel.model.Rezervacija;
import com.kolokvijum.hotel.services.GostService;
import com.kolokvijum.hotel.services.RezervacijaService;

@Controller
@RequestMapping("/api")
public class RezervacijaController {
	@Autowired
	RezervacijaService rezervacijaService;
	
	@Autowired
	GostService gostService;
	
	@RequestMapping(path = "/rezervacije")
	@PreAuthorize("hasAuthority('gost')")
	public ResponseEntity<List<Rezervacija>> pristupRezervacijama(@AuthenticationPrincipal UserDetails userDetails){
		Gost gost = gostService.findByName(userDetails.getUsername());
		return new ResponseEntity<List<Rezervacija>>(rezervacijaService.findMyReservations(gost), HttpStatus.OK);
	}

}
