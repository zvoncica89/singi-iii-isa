package com.kolokvijum.hotel.model;

import javax.persistence.Column;
import javax.persistence.Table;


@javax.persistence.Entity

@Table(name = "soba")
public class Soba {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "soba_id")
    private Long id;
        
    private String naziv;
        
    private String opis;
        
    private Long brojKreveta;
        
    @javax.persistence.OneToMany(mappedBy="soba")
    private java.util.List<Rezervacija> rezervacija;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getNaziv () {
        return this.naziv;
    }

    public void setNaziv (String naziv) {
        this.naziv = naziv;
    }
    
    public String getOpis () {
        return this.opis;
    }

    public void setOpis (String opis) {
        this.opis = opis;
    }
    
    public Long getBrojKreveta () {
        return this.brojKreveta;
    }

    public void setBrojKreveta (Long brojKreveta) {
        this.brojKreveta = brojKreveta;
    }
    
    public java.util.List<Rezervacija> getRezervacija () {
        return this.rezervacija;
    }

    public void setRezervacija (java.util.List<Rezervacija> rezervacija) {
        this.rezervacija = rezervacija;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((brojKreveta == null) ? 0 : brojKreveta.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((naziv == null) ? 0 : naziv.hashCode());
		result = prime * result + ((opis == null) ? 0 : opis.hashCode());
		result = prime * result + ((rezervacija == null) ? 0 : rezervacija.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Soba other = (Soba) obj;
		if (brojKreveta == null) {
			if (other.brojKreveta != null)
				return false;
		} else if (!brojKreveta.equals(other.brojKreveta))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (opis == null) {
			if (other.opis != null)
				return false;
		} else if (!opis.equals(other.opis))
			return false;
		if (rezervacija == null) {
			if (other.rezervacija != null)
				return false;
		} else if (!rezervacija.equals(other.rezervacija))
			return false;
		return true;
	}
    
    

}