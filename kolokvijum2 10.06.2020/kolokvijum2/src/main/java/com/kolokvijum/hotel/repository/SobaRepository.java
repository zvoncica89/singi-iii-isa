package com.kolokvijum.hotel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kolokvijum.hotel.model.Soba;

@Repository
public interface SobaRepository extends CrudRepository<Soba, Long>{

}
