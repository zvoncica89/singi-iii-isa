package com.kolokvijum.hotel.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kolokvijum.hotel.model.Rezervacija;

@Repository
public interface RezervacijaRepository extends CrudRepository<Rezervacija, Long>{

}
