package Parking;


@javax.persistence.Entity

@Table(name = "vlasnik")
public class Vlasnik {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "vlasnik_id")
    private Long id;
        
    private String ime;
        
    private String prezime;
        
    private String korisnickoIme;
        
    private String lozinka;
        
    @javax.persistence.OneToMany(mappedBy="vlasnik")
    private java.util.List<Vozilo> vozilo;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public String getIme () {
        return this.ime;
    }

    public void setIme (String ime) {
        this.ime = ime;
    }
    
    public String getPrezime () {
        return this.prezime;
    }

    public void setPrezime (String prezime) {
        this.prezime = prezime;
    }
    
    public String getKorisnickoIme () {
        return this.korisnickoIme;
    }

    public void setKorisnickoIme (String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }
    
    public String getLozinka () {
        return this.lozinka;
    }

    public void setLozinka (String lozinka) {
        this.lozinka = lozinka;
    }
    
    public java.util.List<Vozilo> getVozilo () {
        return this.vozilo;
    }

    public void setVozilo (java.util.List<Vozilo> vozilo) {
        this.vozilo = vozilo;
    }
    


}