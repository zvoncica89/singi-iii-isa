package parking.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import parking.model.ParkingKarta;

@Repository
public interface ParkingKartaRepository extends PagingAndSortingRepository<ParkingKarta, Long> {

}
