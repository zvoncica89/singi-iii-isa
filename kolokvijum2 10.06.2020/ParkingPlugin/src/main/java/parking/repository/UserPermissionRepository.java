package parking.repository;

import java.util.Set;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import parking.model.UserPermission;


@Repository
public interface UserPermissionRepository extends CrudRepository<UserPermission, Long>{
	Set<UserPermission> getByUserId(Long id);
}
