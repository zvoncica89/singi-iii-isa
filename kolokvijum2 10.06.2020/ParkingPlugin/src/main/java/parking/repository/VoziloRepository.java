package parking.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import parking.model.Vozilo;

@Repository
public interface VoziloRepository extends PagingAndSortingRepository<Vozilo, Long> {

}
