package parking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import parking.model.Permission;


@Repository
public interface PermissionRepository extends CrudRepository<Permission, Long>{

}
