package parking.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import parking.model.Vlasnik;

@Repository
public interface VlasnikReposiroty extends PagingAndSortingRepository<Vlasnik, Long> {

}
