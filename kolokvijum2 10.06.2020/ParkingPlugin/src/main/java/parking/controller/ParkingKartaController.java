package parking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import parking.model.ParkingKarta;

@Controller
@RequestMapping(path = "/api/parkingKarta")
@CrossOrigin(origins = "*")
public class ParkingKartaController extends CRUDController<ParkingKarta, Long> {

}
