package parking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import parking.model.Vozilo;

@Controller
@RequestMapping(path = "/api/vozilo")
@CrossOrigin(origins = "*")
public class VoziloController extends CRUDController<Vozilo, Long> {
	

}
