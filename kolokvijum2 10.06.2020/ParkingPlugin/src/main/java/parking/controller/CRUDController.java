package parking.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import parking.DTO.DTO;
import parking.model.CreateDTO;
import parking.service.CRUDService;

public class CRUDController <T extends CreateDTO, ID> {
	@Autowired
	protected CRUDService<T, ID> crudService;
	
	public CRUDService<T, ID> getCrudService(){
		return this.crudService;
	}
	public void setCrudService(CRUDService<T, ID> crudService) {
		this.crudService = crudService;
	}
	
	@RequestMapping(path="", method = RequestMethod.GET)
	public ResponseEntity<Page<DTO>> findAll(Pageable pageable){
		Page<DTO> page = this.crudService.findAll(pageable).map(t -> t.getDTO(false));
		return new ResponseEntity<Page<DTO>> (page, HttpStatus.OK);
	}
	
	@RequestMapping(path="/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> findOne(@PathVariable("id") ID id) {
		Optional<T> t = this.crudService.findOne(id);
		if(t.isPresent()) {
			return new ResponseEntity<DTO>(t.get().getDTO(false), HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
		
	}
	
	@RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> create(@RequestBody T noviEntitet) {
//	check for id
	if (noviEntitet.getId() != null) {
//		id exists, lets try to find model with given id
	    Optional<T> t = this.crudService.findOne((ID)noviEntitet.getId());

	    if (t.isEmpty()) {
//			we didn't find model, lets create one and return DTO
		this.crudService.save(noviEntitet);
		return new ResponseEntity<DTO>(noviEntitet.getDTO(false), HttpStatus.CREATED);
	    } else {
//			model is found, lets return HttpStatus.CONFLICT
		return new ResponseEntity<Void>(HttpStatus.CONFLICT);
	    }
	} else {
//		id doesn't exist, so model doesn't exist lets create one and return DTO
	    this.crudService.save(noviEntitet);
	    return new ResponseEntity<DTO>(noviEntitet.getDTO(false), HttpStatus.CREATED);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody T noviEnitet) {
//	check for id
	if (noviEnitet.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.crudService.findOne((ID)noviEnitet.getId());

	    if (t.isPresent()) {
//			model is found, lets update it and return DTO
		this.crudService.save(noviEnitet);
		return new ResponseEntity<DTO>(t.get().getDTO(false), HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") ID id) {
//	try to find model
	Optional<T> t = this.crudService.findOne(id);

	if (t.isPresent()) {
//		model is found, lets delete it and return HttpStatus.OK
	    this.crudService.delete(t.get());
	    return new ResponseEntity<Void>(HttpStatus.OK);
	} else {
//		we didn't find model, lets return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

    @RequestMapping(path = "", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@RequestBody T noviEnitet) {
//	check for id
	if (noviEnitet.getId() != null) {
//		id exists, lets try to find model
	    Optional<T> t = this.crudService.findOne((ID)noviEnitet.getId());

	    if (t.isPresent()) {
//			model is found, lets delete it and return HttpStatus.OK
		this.crudService.delete(t.get());
		return new ResponseEntity<Void>(HttpStatus.OK);
	    } else {
//			we didn't find model, lets return HttpStatus.NOT_FOUND
		return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	    }
	} else {
//		id doesn't exists, let return HttpStatus.NOT_FOUND
	    return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
	}
    }

}
