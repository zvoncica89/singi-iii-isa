package parking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import parking.model.Vlasnik;

@Controller
@RequestMapping(path = "/api/vlasnik")
@CrossOrigin(origins = "*")
public class VlasnikController extends CRUDController<Vlasnik, Long>{
	
}
