package parking.DTO;

import java.util.List;
import java.util.stream.Collectors;

import parking.model.Vlasnik;

public class VlasnikDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String ime;
	private String prezime;
	private String korisnickoIme;
	private String lozinka;
	private List<VoziloDTO> vozilo;
	public VlasnikDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public VlasnikDTO(Long id, String ime, String prezime, String korisnickoIme, String lozinka,
			List<VoziloDTO> vozilo) {
		super();
		this.id = id;
		this.ime = ime;
		this.prezime = prezime;
		this.korisnickoIme = korisnickoIme;
		this.lozinka = lozinka;
		this.vozilo = vozilo;
	}
	
	public VlasnikDTO(Vlasnik vlasnik, boolean isInsideDto) {
		this.id = vlasnik.getId();
		this.ime = vlasnik.getIme();
		this.prezime = vlasnik.getPrezime();
		this.korisnickoIme = vlasnik.getKorisnickoIme();
		this.lozinka = vlasnik.getLozinka();
		if (!isInsideDto) {
			this.vozilo = vlasnik.getVozilo().stream().map(v -> v.getDTO(!isInsideDto)).collect(Collectors.toList());
		}
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public List<VoziloDTO> getVozilo() {
		return vozilo;
	}
	public void setVozilo(List<VoziloDTO> vozilo) {
		this.vozilo = vozilo;
	}
	
	
	
}
