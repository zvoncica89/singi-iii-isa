package parking.DTO;

import java.time.LocalDateTime;

import parking.model.ParkingKarta;

public class ParkingKartaDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private Long parkingMesto;
	private LocalDateTime pocetakVazenja;
	private LocalDateTime krajVazenja;
	private VoziloDTO vozilo;

	public ParkingKartaDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ParkingKartaDTO(Long id, Long parkingMesto, LocalDateTime pocetakVazenja, LocalDateTime krajVazenja,
			VoziloDTO vozilo) {
		super();
		this.id = id;
		this.parkingMesto = parkingMesto;
		this.pocetakVazenja = pocetakVazenja;
		this.krajVazenja = krajVazenja;
		this.vozilo = vozilo;
	}

	public ParkingKartaDTO(ParkingKarta parkignKarta, boolean isInsideDto) {
		this.id = parkignKarta.getId();
		this.parkingMesto = parkignKarta.getParkingMesto();
		this.pocetakVazenja = parkignKarta.getPocetakVazenja();
		this.krajVazenja = parkignKarta.getKrajVazenja();
		if (!isInsideDto) {
			this.vozilo = parkignKarta.getVozilo().getDTO(!isInsideDto);
		}

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getParkingMesto() {
		return parkingMesto;
	}

	public void setParkingMesto(Long parkingMesto) {
		this.parkingMesto = parkingMesto;
	}

	public LocalDateTime getPocetakVazenja() {
		return pocetakVazenja;
	}

	public void setPocetakVazenja(LocalDateTime pocetakVazenja) {
		this.pocetakVazenja = pocetakVazenja;
	}

	public LocalDateTime getKrajVazenja() {
		return krajVazenja;
	}

	public void setKrajVazenja(LocalDateTime krajVazenja) {
		this.krajVazenja = krajVazenja;
	}

	public VoziloDTO getVozilo() {
		return vozilo;
	}

	public void setVozilo(VoziloDTO vozilo) {
		this.vozilo = vozilo;
	}

}
