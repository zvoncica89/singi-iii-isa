package parking.DTO;


public class UserDto {
	private String username;

	public UserDto() {
		super();
	}

	public UserDto(String username) {
		super();
		this.username = username;
	}
	
	public UserDto(parking.model.User user) {
		this(user.getUsername());
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
