package parking.DTO;

import java.util.List;
import java.util.stream.Collectors;

import parking.model.Vozilo;

public class VoziloDTO implements DTO {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long id;
	private String registracioniBroj;
	private VlasnikDTO vlasnik;
	private List<ParkingKartaDTO> parkingKarta;
	public VoziloDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	public VoziloDTO(Long id, String registracioniBroj, VlasnikDTO vlasnik, List<ParkingKartaDTO> parkingKarta) {
		super();
		this.id = id;
		this.registracioniBroj = registracioniBroj;
		this.vlasnik = vlasnik;
		this.parkingKarta = parkingKarta;
	}
	
	public VoziloDTO(Vozilo vozilo, boolean isInsideDTO) {
		this.id = vozilo.getId();
		this.registracioniBroj = vozilo.getRegistracioniBroj();
		if(!isInsideDTO) {
			this.vlasnik = vozilo.getVlasnik().getDTO(!isInsideDTO);
			this.parkingKarta = vozilo.getParkignKarta().stream().map(p -> p.getDTO(!isInsideDTO)).collect(Collectors.toList());
		}
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getRegistracioniBroj() {
		return registracioniBroj;
	}
	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}
	public VlasnikDTO getVlasnik() {
		return vlasnik;
	}
	public void setVlasnik(VlasnikDTO vlasnik) {
		this.vlasnik = vlasnik;
	}
	public List<ParkingKartaDTO> getParkingKarta() {
		return parkingKarta;
	}
	public void setParkingKarta(List<ParkingKartaDTO> parkingKarta) {
		this.parkingKarta = parkingKarta;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
