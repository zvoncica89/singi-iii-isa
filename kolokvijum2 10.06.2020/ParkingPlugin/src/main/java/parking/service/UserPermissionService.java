package parking.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import parking.model.UserPermission;
import parking.repository.UserPermissionRepository;


@Service
public class UserPermissionService {
	@Autowired
	private UserPermissionRepository userPermissionRepository;
	
	public Set<UserPermission> getPermissionsByUserId(Long id) {
		return userPermissionRepository.getByUserId(id);
	}
}
