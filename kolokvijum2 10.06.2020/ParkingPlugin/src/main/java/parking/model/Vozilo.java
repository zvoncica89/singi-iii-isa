package parking.model;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import parking.DTO.VoziloDTO;

@javax.persistence.Entity

@Table(name = "vozilo")
public class Vozilo implements CreateDTO<Long>, Identitet {

	@javax.persistence.Id
	@javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
	@Column(name = "vozilo_id")
	private Long id;

	private String registracioniBroj;

	@javax.persistence.ManyToOne
	@JoinColumn(name = "vlasnik_id")
	private Vlasnik vlasnik;

	@javax.persistence.OneToMany(mappedBy = "vozilo")
	private java.util.List<ParkingKarta> parkignKarta;

	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRegistracioniBroj() {
		return this.registracioniBroj;
	}

	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}

	public Vlasnik getVlasnik() {
		return this.vlasnik;
	}

	public void setVlasnik(Vlasnik vlasnik) {
		this.vlasnik = vlasnik;
	}

	public java.util.List<ParkingKarta> getParkignKarta() {
		return this.parkignKarta;
	}

	public void setParkignKarta(java.util.List<ParkingKarta> parkignKarta) {
		this.parkignKarta = parkignKarta;
	}

	@Override
	public VoziloDTO getDTO(boolean isInsideDTO) {
		VoziloDTO voziloDTO = new VoziloDTO(this, isInsideDTO);
		return voziloDTO;
	}

}