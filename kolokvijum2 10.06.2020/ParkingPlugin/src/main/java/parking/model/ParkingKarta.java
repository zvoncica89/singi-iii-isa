package parking.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import parking.DTO.ParkingKartaDTO;

@javax.persistence.Entity

@Table(name = "parkign_karta")
public class ParkingKarta implements CreateDTO<Long>, Identitet {

    @javax.persistence.Id
    @javax.persistence.GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    @Column(name = "parkign_karta_id")
    private Long id;
        
    private Long parkingMesto;
        
    private LocalDateTime pocetakVazenja;
        
    private LocalDateTime krajVazenja;
        
    @javax.persistence.ManyToOne
    @JoinColumn(name = "vozilo_id")
    private Vozilo vozilo;
        


    public Long getId () {
        return this.id;
    }

    public void setId (Long id) {
        this.id = id;
    }
    
    public Long getParkingMesto () {
        return this.parkingMesto;
    }

    public void setParkingMesto (Long parkingMesto) {
        this.parkingMesto = parkingMesto;
    }
    
    public LocalDateTime getPocetakVazenja () {
        return this.pocetakVazenja;
    }

    public void setPocetakVazenja (LocalDateTime pocetakVazenja) {
        this.pocetakVazenja = pocetakVazenja;
    }
    
    public LocalDateTime getKrajVazenja () {
        return this.krajVazenja;
    }

    public void setKrajVazenja (LocalDateTime krajVazenja) {
        this.krajVazenja = krajVazenja;
    }
    
    public Vozilo getVozilo () {
        return this.vozilo;
    }

    public void setVozilo (Vozilo vozilo) {
        this.vozilo = vozilo;
    }

	@Override
	public ParkingKartaDTO getDTO(boolean isInsideDTO) {
		ParkingKartaDTO parkingKartaDTO = new ParkingKartaDTO(this, isInsideDTO);
		return parkingKartaDTO;
	}
    


}