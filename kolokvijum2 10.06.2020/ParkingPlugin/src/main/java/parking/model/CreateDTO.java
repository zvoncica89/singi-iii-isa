package parking.model;

import parking.DTO.DTO;

public interface CreateDTO<ID> {
	public DTO getDTO(boolean isInsideDTO);
	public ID getId();

}
